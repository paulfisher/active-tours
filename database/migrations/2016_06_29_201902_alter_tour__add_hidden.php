<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourAddHidden extends Migration {

    public function up() {
        Schema::table('tour', function (Blueprint $table) {
            $table->boolean('is_hidden')->default(false);
        });
    }

    public function down() {
        Schema::table('tour', function (Blueprint $table) {
            $table->dropColumn('is_hidden');
        });
    }

}