<?php

use App\Models\Booking;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBookingAddOwnerInfo extends Migration {

    public function up() {
        Booking::getQuery()->delete();
        Schema::table('booking', function (Blueprint $table) {
            $table->integer('tour_user_id')->unsigned();

            $table->foreign('tour_user_id')
                ->references('id')
                ->on('user')
                ->onDelete('restrict');
        });
    }

    public function down() {
        Schema::table('booking', function (Blueprint $table) {
            $table->dropForeign('booking_tour_user_id_foreign');
            $table->dropColumn('tour_user_id');
        });
    }
}