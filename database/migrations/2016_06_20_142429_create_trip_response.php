<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripResponse extends Migration {


    public function up()
    {
        Schema::create('trip_response', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id')->unsigned();

            $table->string('title');
            $table->string('image_src');
            $table->string('content');

            $table->foreign('trip_id')
                ->references('id')
                ->on('trip')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('trip_response');
    }

}
