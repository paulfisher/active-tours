<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourAddPriceInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour', function (Blueprint $table) {
            $table->text('price_includes')->nullable();
            $table->text('price_excludes')->nullable();
            $table->tinyInteger('complexity')->default(0);
            $table->text('url_about')->nullable();
            $table->text('capacity')->nullable();
        });

        Schema::create('tour_image', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned();
            $table->string('src');

            $table->foreign('tour_id')
                ->references('id')
                ->on('tour')
                ->onDelete('cascade');
        });

        Schema::create('tour_day', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned();
            $table->text('description');

            $table->foreign('tour_id')
                ->references('id')
                ->on('tour')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour', function (Blueprint $table) {
            $table->dropColumn('capacity');
            $table->dropColumn('url_about');
            $table->dropColumn('complexity');
            $table->dropColumn('price_excludes');
            $table->dropColumn('price_includes');
        });

        Schema::drop('tour_image');
        Schema::drop('tour_day');
    }
}
