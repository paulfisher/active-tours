<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourImage extends Migration {

    public function up() {
        Schema::table('tour_image', function (Blueprint $table) {
            $table->string('small_src')->nullable();
            $table->string('medium_src')->nullable();
            $table->string('large_src')->nullable();
        });
    }

    public function down() {
        Schema::table('tour_image', function (Blueprint $table) {
            $table->dropColumn('small_src');
            $table->dropColumn('medium_src');
            $table->dropColumn('large_src');
        });
    }
}
