<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTourAddPrice extends Migration {

    public function up() {
        Schema::table('tour', function (Blueprint $table) {
            $table->decimal('price')->nullable();
        });
    }

    public function down() {
        Schema::table('tour', function (Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
