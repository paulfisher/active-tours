<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserAddSmsEnabled extends Migration {

    public function up() {
        Schema::table('user', function (Blueprint $table) {
            $table->boolean('is_sms_enabled')->default(false);
            $table->boolean('is_phone_number_confirmed')->default(false);
            $table->string('phone_number_confirm_code')->nullable();
            $table->unique('phone_number');
        });
    }

    public function down() {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('is_sms_enabled');
            $table->dropColumn('is_phone_number_confirmed');
            $table->dropColumn('phone_number_confirm_code');
        });
    }
}
