<?php

use App\Models\Tour;
use App\Models\Trip;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserAddIsSeller extends Migration {

    public function up() {
        Schema::table('user', function (Blueprint $table) {
            $table->boolean('is_seller')->default(false);
        });

        Trip::getQuery()->delete();
        Schema::table('trip', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade');
        });
    }


    public function down() {
        Schema::table('user', function (Blueprint $table) {
            $table->dropColumn('is_seller');
        });

        Schema::table('trip', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
