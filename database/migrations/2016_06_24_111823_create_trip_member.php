<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripMember extends Migration {

    public function up() {
        Schema::create('booking', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('trip_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();

            $table->tinyInteger('status');

            $table->timestamp('start_at')->nullable();
            $table->timestamp('finish_at')->nullable();

            $table->decimal('price');
            $table->decimal('paid_value')->default(0);

            $table->integer('members')->default(0);
            $table->longText('description')->nullable();

            $table->foreign('trip_id')
                ->references('id')
                ->on('trip')
                ->onDelete('set null');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('booking');
    }
}