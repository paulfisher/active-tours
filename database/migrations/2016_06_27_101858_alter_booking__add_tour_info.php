<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBookingAddTourInfo extends Migration {

    public function up() {
        Schema::table('booking', function (Blueprint $table) {
            $table->string('tour_name')->nullable();
            $table->text('tour_excerpt')->nullable();
            $table->text('tour_description')->nullable();
            $table->smallInteger('tour_category')->nullable();
            $table->text('tour_price_includes')->nullable();
            $table->text('tour_price_excludes')->nullable();
            $table->tinyInteger('tour_complexity')->default(0);
            $table->text('tour_url_about')->nullable();
            $table->text('tour_capacity')->nullable();
            $table->string('tour_area_name')->nullable();
        });
    }

    public function down() {
        Schema::table('booking', function (Blueprint $table) {
            $table->dropColumn('tour_name');
            $table->dropColumn('tour_excerpt');
            $table->dropColumn('tour_description');
            $table->dropColumn('tour_category');
            $table->dropColumn('tour_price_includes');
            $table->dropColumn('tour_price_excludes');
            $table->dropColumn('tour_complexity');
            $table->dropColumn('tour_url_about');
            $table->dropColumn('tour_capacity');
            $table->dropColumn('tour_area_name');
        });
    }
}
