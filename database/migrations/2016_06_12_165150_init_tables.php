<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitTables extends Migration {

    public function up() {

        //Страна
        Schema::create('country', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->index();
            $table->integer('vk_id')->index();
        });

        //Регион
        Schema::create('region', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('vk_id')->index();
            $table->string('name')->index();

            $table->foreign('country_id')
                ->references('id')
                ->on('country')
                ->onDelete('cascade');
        });

        //Город
        Schema::create('city', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('country_id')->unsigned();
            $table->integer('vk_id')->index();
            $table->string('name')->index();
            $table->string('region');
            $table->string('area');
            $table->boolean('important');

            $table->foreign('country_id')
                ->references('id')
                ->on('country')
                ->onDelete('cascade');
        });

        //Район, например Алтай, Карелия, Камчатка
        Schema::create('area', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique()->index();
        });

        Schema::create('tour', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();//какой пользователь создал тур
            $table->integer('area_id')->nullable()->unsigned()->index();//регион тура

            $table->string('name');
            $table->text('excerpt');
            $table->text('description');
            $table->smallInteger('category')->nullable()->index();

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onDelete('cascade');

            $table->foreign('area_id')
                ->references('id')
                ->on('area')
                ->onDelete('set null');

            $table->timestamps();
        });

        Schema::create('trip', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tour_id')->unsigned()->index();

            $table->timestamp('start_at')->nullable();
            $table->timestamp('finish_at')->nullable();
            $table->decimal('price');

            $table->foreign('tour_id')
                ->references('id')
                ->on('tour')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::drop('trip');
        Schema::drop('tour');
        Schema::drop('area');
        Schema::drop('city');
        Schema::drop('region');
        Schema::drop('country');
    }
}
