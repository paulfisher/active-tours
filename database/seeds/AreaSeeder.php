<?php

use App\Models\Area;
use Illuminate\Database\Seeder;

class AreaSeeder extends Seeder {

    public function run() {
        $this->insertOrExists('Алтай', 'altai', 'на Алтай', 'Алтай — это обобщенное название Алтайского края и Республики Алтай, которые в данный момент представляют собой два самостоятельных региона. Республика Алтай, территория которой занимает всего лишь 260 кв. км, находится на юге Западной Сибири и является частью Сибирского Федерального Округа. Расположена у истоков реки Обь, которая образована слиянием двух рек — Катунь и Бия — крупнейшими водными артериями, берущими начало в республике. Многие называют эту область Горный Алтай, т. к. она представлена горными хребтами. Высшая точка этой горной системы — гора Белуха, высота которой составляет 4509 м. Белуха является высочайшей в Сибири, очень интересна в альпинистском плане. Столица — город Горно-Алтайск, единственный город этой маленькой республики. Численность населения на 1 января 2012 г. составляла 208 445 человек, где коренное население — алтайцы, потомки тюркских племен. Религиозные воззрения представлены шаманизмом, бурханизмом, православием, буддизмом, исламом. Главной автомагистралью региона является Чуйский тракт (федеральная трасса М-52), как бы разрезающий Горный Алтай пополам. Начинающийся в Новосибирске, он дает выход России в сопредельную Монголию; общая протяженность тракта составляет 963 км. В 1998 г. в список ЮНЕСКО были включены пять уникальных природных объектов под общим названием «Алтай — Золотые Горы». Это Телецкое озеро, Алтайский Государственный Природный Заповедник, Зона Покоя «Укок», Катунский Государственный Природный Биосферный Заповедник и гора Белуха. Эти объекты — мировое природное наследие с неповторимыми этнокультурными, экологическими характеристиками и особым разнообразием флоры и фауны.');
        $this->insertOrExists('Карелия', 'karelia', 'в Карелию', 'Западная граница Карелии совпадает с государственной границей Российской Федерации и Финляндии, имеет протяжённость 798,3 км, одновременно являясь границей с Европейским Союзом. На востоке Карелия граничит с Архангельской областью, на юге — с Вологодской и Ленинградской областями, на севере — с Мурманской областью.');
        $this->insertOrExists('Камчатка', 'kamchatka', 'на Камчатку', '');
        $this->insertOrExists('Кавказ', 'kavkaz', 'на Кавказ', '');
        $this->insertOrExists('Урал', 'ural', 'на Урал', '');
    }

    private function insertOrExists($name, $slug, $directionForm, $about) {
        $exists = Area::where('slug', $slug)->first();
        if (!$exists) {
            Area::create([
                'slug' => $slug,
                'name' => $name,
                'direction_form' => $directionForm,
                'about' => $about
            ]);
        }
    }
}
