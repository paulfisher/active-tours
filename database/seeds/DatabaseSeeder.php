<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    public function run() {
        //$this->call(VkGeoSeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(InitSeeder::class);
    }

}