<?php

use App\Models\Area;
use App\Models\Tour;
use App\Models\Trip;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = User::where('email', 'pavel.rybako@gmail.com')->first();
        if (!$user) {
            $user = User::create([
                'name' => 'pavel.rybako',
                'email' => 'pavel.rybako@gmail.com',
                'password' => '1',
            ]);
        }

        /** @var Area $area */
        $area = Area::where('slug', 'altai')->first();
        if (!$area) {
            $area = Area::create([
                'slug' => 'altai',
                'name' => 'Алтай',
            ]);
        }


        $name = 'Восхождение на Эверест';
        /** @var Tour $tour */
        $tour = Tour::where('name', $name)->first();
        if (!$tour) {
            $tour = Tour::create([
                'user_id' => $user->id,
                'area_id' => $area->id,
                'name' => 'Восхождение на Эверест',
                'excerpt' => 'Восхождение на Эверест для самых отважных енотов',
                'description' => 'Восхождение на Эверест для самых отважных енотов полоскунов',
                'category' => 1,
                'price_includes' => 'Проживание, питание, экипировка',
                'price_excludes' => 'Проезд до места старта',
                'complexity' => 5,
                'url_about' => 'http://hhiker.ru',
                'capacity' => 'от 2 до 10 человек',
            ]);


            $tour->days()->create(['description' => 'Заезд в гостиницу']);
            $tour->days()->create(['description' => 'Сплав']);
            $tour->days()->create(['description' => 'Выезд']);

            $tour->images()->create(['src' => '/images/tour/karelia2.jpeg']);
            $tour->images()->create(['src' => '/images/tour/kamchatka.jpg']);
            $tour->images()->create(['src' => '/images/tour/kamchatka2.jpg']);
            $tour->images()->create(['src' => '/images/tour/kavkaz.jpg']);
            $tour->images()->create(['src' => '/images/tour/ural.jpg']);
        }


        if (!$tour->trips()->exists()) {
            /** @var Trip $trip */
            $trip = $tour->trips()->create([
                'price' => '10000',
                'start_at' => Carbon::now(),
                'finish_at' => Carbon::now()->addDays(4),
                'user_id' => $user->id
            ]);

            $trip->responses()->create([
                'title' => 'Валентина',
                'image_src' => '/images/face3.jpg',
                'content' => 'Все прошло хорошо, программа очень насыщенная, скучать не приходилось. На Телецкрм застали зиму, далее, близ Горноалтайска, была уже весна и отличная погода. Программой довольны.'
            ]);

            $trip->responses()->create([
                'title' => 'Игорь',
                'image_src' => '/images/face.jpg',
                'content' => 'Все прошло хорошо, программа очень насыщенная, скучать не приходилось. На Телецкрм застали зиму, далее, близ Горноалтайска, была уже весна и отличная погода. Программой довольны.'
            ]);

            $trip->responses()->create([
                'title' => 'Оля',
                'image_src' => '/images/face2.jpg',
                'content' => 'Все прошло хорошо, программа очень насыщенная, скучать не приходилось. На Телецкрм застали зиму, далее, близ Горноалтайска, была уже весна и отличная погода. Программой довольны.'
            ]);
        }
    }
}
