<?php

return [
    'categories' => [
        'rafting' => [
            'id' => 1,
            'name' => 'Рафтинг'
        ],
        'horse' => [
            'id' => 2,
            'name' => 'Конный'
        ],
        'hiking' => [
            'id' => 3,
            'name' => 'Пеший'
        ],
        'auto' => [
            'id' => 4,
            'name' => 'Авто'
        ],
        'bicycle' => [
            'id' => 5,
            'name' => 'Вело'
        ],
        'quadro' => [
            'id' => 6,
            'name' => 'Квадроциклы'
        ],
        'speleo' => [
            'id' => 7,
            'name' => 'Спелео'
        ],
        'hunting' => [
            'id' => 8,
            'name' => 'Охота'
        ],
        'fishing' => [
            'id' => 9,
            'name' => 'Рыбалка'
        ],
        'mountain' => [
            'id' => 10,
            'name' => 'Горный'
        ],
        'winter' => [
            'id' => 11,
            'name' => 'Зимний'
        ],
    ]
];