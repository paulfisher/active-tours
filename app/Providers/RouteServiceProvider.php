<?php

namespace App\Providers;

use App\Models\Area;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourDay;
use App\Models\TourImage;
use App\Models\Trip;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);


        $router->bind('tour', function($id) {
            return Tour::findOrFail($id);
        });

        $router->bind('trip', function($id) {
            return Trip::find($id);
        });

        $router->bind('booking', function($id) {
            return Booking::find($id);
        });

        $router->bind('tour_image', function($id) {
            return TourImage::find($id);
        });

        $router->bind('day', function($id) {
            return TourDay::find($id);
        });

        $router->bind('area', function($id) {
            if (is_numeric($id)) {
                $byId = Area::find($id);
                if ($byId) {
                    return $byId;
                }
            }
            return Area::where('slug', $id)->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
