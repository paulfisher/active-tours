<?php

namespace App\Providers;

use App\Services\AreaService;
use App\Services\BookingService;
use App\Services\CategoryService;
use App\Services\ImageService;
use App\Services\NotifyService;
use App\Services\Sms\SmsSender;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider{

    public function boot() {
        setlocale(LC_TIME, config('app.locale'));
    }

    public function register() {
        $this->app->singleton(CategoryService::class);
        $this->app->singleton(AreaService::class);
        $this->app->singleton(BookingService::class);
        $this->app->singleton(ImageService::class);
        $this->app->singleton(SmsSender::class);
        $this->app->singleton(NotifyService::class);
        $this->app->singleton(UserService::class);
    }

}