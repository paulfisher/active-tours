<?php

namespace App\Models;

use App\Services\CategoryService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * @property integer id
 * @property User user
 * @property Trip trip
 * @property integer tour_category
 * @property integer status
 * @property float price
 * @property float payment_value
 * @property Carbon created_at
 * @property User tourUser
 */
class Booking extends Model {

    const PENDING = 1;
    const CONFIRMED = 2;
    const CANCEL = 3;
    const PAID = 4;

    protected $table = 'booking';

    protected $fillable = [
        'trip_id',
        'user_id',
        'tour_user_id',
        'status',
        'start_at',
        'finish_at',
        'price',
        'payment_value',
        'paid_value',
        'members',
        'description',
        'tour_name',
        'tour_excerpt',
        'tour_description',
        'tour_category',
        'tour_price_includes',
        'tour_price_excludes',
        'tour_complexity',
        'tour_url_about',
        'tour_capacity',
        'tour_area_name',
    ];

    protected $dates = ['start_at', 'finish_at'];

    public function trip() {
        return $this->belongsTo('App\Models\Trip');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function tourUser() {
        return $this->belongsTo('App\Models\User', 'tour_user_id');
    }

    public function getTourCategoryNameAttribute() {
        $category = App::make(CategoryService::class)->getCategory($this->tour_category);
        if ($category && isset($category['name'])) {
            return $category['name'];
        }
        return '';
    }

    public function getStatusNameAttribute() {
        switch ($this->status) {
            case self::PENDING: return 'создана';
            case self::CONFIRMED: return 'подтверждена';
            case self::CANCEL: return 'отменена';
            case self::PAID: return 'оплачена';
        }
        return '';
    }

    public function isPending() {
        return $this->status == self::PENDING;
    }

    public function isConfirmed() {
        return $this->status == self::CONFIRMED;
    }

    public function isCancel() {
        return $this->status == self::CANCEL;
    }

    public function isPaid() {
        return $this->status == self::PAID;
    }
}
