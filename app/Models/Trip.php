<?php

namespace App\Models;

use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Tour tour
 * @property integer id
 * @property Carbon start_at
 * @property Carbon finish_at
 * @property float price
 */
class Trip extends Model {

    protected $table = 'trip';

    protected $fillable = [
        'tour_id',
        'user_id',
        'start_at',
        'finish_at',
        'price',
    ];

    protected $dates = ['start_at', 'finish_at'];

    public function tour() {
        return $this->belongsTo('App\Models\Tour');
    }

    public function responses() {
        return $this->hasMany('App\Models\TripResponse');
    }
}
