<?php

namespace App\Models;

use App\Scopes\TourScope;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User user
 * @property string name
 * @property integer id
 * @property string excerpt
 * @property string description
 * @property integer category
 * @property string price_includes
 * @property string price_excludes
 * @property integer complexity
 * @property string url_about
 * @property string capacity
 * @property boolean is_hidden
 *
 * @property Area area
 *
 */
class Tour extends Model {

    protected $table = 'tour';

    protected $fillable = [
        'user_id',
        'area_id',
        'name',
        'excerpt',
        'description',
        'category',
        'price_includes',
        'price_excludes',
        'complexity',
        'url_about',
        'capacity',
        'price',
        'is_hidden',
    ];

    public function comments() {
        return $this->hasMany('App\Models\Trip');
    }

    public function area() {
        return $this->belongsTo('App\Models\Area');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function trips() {
        return $this->hasMany('App\Models\Trip');
    }

    public function images() {
        return $this->hasMany('App\Models\TourImage');
    }

    public function days() {
        return $this->hasMany('App\Models\TourDay');
    }

    public function bookings() {
        return $this->hasManyThrough('App\Models\Booking', 'App\Models\Trip');
    }

    public function scopeActive($query) {
        return $query->where('is_hidden', false);
    }
}
