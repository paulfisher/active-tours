<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Region extends Model {

    protected $table = 'region';

    public $timestamps = false;

    protected $fillable = [
        'country_id',
        'vk_id',
        'name',
    ];

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }
}
