<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class City extends Model
{
    protected $table = 'city';

    public $timestamps = false;

    protected $fillable = [
        'country_id',
        'vk_id',
        'name',
        'region',
        'area'
    ];

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }
}
