<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Area id
 * @property string slug
 * @property string name
 */
class Area extends Model {

    protected $table = 'area';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'slug',
        'direction_form',
        'about'
    ];

    public function tours() {
        return $this->hasMany('App\Models\Tour');
    }
}
