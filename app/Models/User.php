<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property integer id
 * @property string email
 * @property boolean is_seller
 * @property string name
 * @property string phone_number
 * @property boolean is_sms_enabled
 * @property string phone_number_confirm_code
 * @property string is_phone_number_confirmed
 */
class User extends Authenticatable {

    protected $table = 'user';

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone_number',
        'is_seller',
        'is_sms_enabled',
        'is_phone_number_confirmed',
        'phone_number_confirm_code'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function generatePhoneNumberConfirmCode() {
        return rand(1000, 9999);
    }

    public function bookings() {
        return $this->hasMany('App\Models\Booking');
    }

    public function tours() {
        return $this->hasMany('App\Models\Tour');
    }

    public function sales() {
        return $this->hasManyThrough('App\Models\Booking', 'App\Models\Trip');
    }
}