<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourDay extends Model {

    protected $table = 'tour_day';

    public $timestamps = false;

    protected $fillable = [
        'tour_id',
        'title',
        'description'
    ];

    public function tour() {
        return $this->belongsTo('App\Models\Tour');
    }
}
