<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourImage extends Model {

    protected $table = 'tour_image';

    public $timestamps = false;

    protected $fillable = [
        'tour_id',
        'src',
        'small_src',
        'medium_src',
        'large_src',
    ];

    public function tour() {
        return $this->belongsTo('App\Models\Tour');
    }
}
