<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TripResponse extends Model {

    protected $table = 'trip_response';

    protected $fillable = [
        'trip_id',
        'title',
        'image_src',
        'content'
    ];

    public function tour() {
        return $this->belongsTo('App\Models\Tour');
    }
}
