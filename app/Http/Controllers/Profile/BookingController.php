<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class BookingController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        return view('profile.bookings.index', compact('user'));
    }

    public function show(Booking $booking) {
        $user = Auth::user();
        return view('profile.bookings.show', compact('booking', 'user'));
    }
}