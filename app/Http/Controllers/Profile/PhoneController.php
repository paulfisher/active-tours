<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\User;
use App\Services\Sms\SmsSender;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class PhoneController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        /** @var User $user */
        $user = Auth::user();
        return view('profile.phone.index', compact('user'));
    }

    public function confirm() {
        $user = Auth::user();
        return view('profile.phone.confirm', compact('user'));
    }

    public function postConfirm() {
        /** @var User $user */
        $user = Auth::user();
        $code = Input::get('code');
        if ($user->phone_number_confirm_code == $code) {
            $user->update(['is_phone_number_confirmed' => true]);
        }
        return redirect()->back();
    }

    public function newCode(SmsSender $smsSender) {
        /** @var User $user */
        $user = Auth::user();
        $user->update([
            'phone_number_confirm_code' => User::generatePhoneNumberConfirmCode()
        ]);
        $smsSender->sendCodeToUser($user, $user->phone_number_confirm_code);
        return redirect()->back();
    }
}