<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourDayRequest;
use App\Models\Tour;
use App\Models\TourDay;
use Illuminate\Support\Facades\Auth;

class TourDayController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function create(Tour $tour) {
        $user = Auth::user();
        return view('profile.tours.day.create', compact('user', 'tour'));
    }

    public function postCreate(Tour $tour, TourDayRequest $tourDayRequest) {
        $tour->days()->create($tourDayRequest->all());
        return redirect()->route('profile.tours.edit', $tour->id);
    }

    public function edit(Tour $tour, TourDay $tourDay) {
        $user = Auth::user();
        return view('profile.tours.day.edit', compact('user', 'tour', 'tourDay'));
    }

    public function update(Tour $tour, TourDay $tourDay, TourDayRequest $tourDayRequest) {
        $tourDay->update($tourDayRequest->all());
        return redirect()->route('profile.tours.edit', $tour->id);
    }

    public function deleteDay(Tour $tour, TourDay $tourDay) {
        $tourDay->delete();
        return redirect()->back();
    }
}