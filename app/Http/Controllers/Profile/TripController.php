<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class TripController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function create(Tour $tour) {
        $user = Auth::user();
        return view('profile.tours.trip.create', compact('user', 'tour'));
    }

    public function postCreate(Tour $tour, TripRequest $tripRequest) {
        $user = Auth::user();
        $tour->trips()->create(
            collect($tripRequest->all())
                ->merge(['user_id'=>$user->id])
                ->all()
        );
        return redirect()->route('profile.tours.edit', $tour->id);
    }

    public function edit(Tour $tour, Trip $trip) {
        $user = Auth::user();
        return view('profile.tours.trip.edit', compact('user', 'tour', 'trip'));
    }

    public function update(Tour $tour, Trip $trip, TripRequest $tripRequest) {
        $trip->update($tripRequest->all());
        return redirect()->route('profile.tours.edit', $tour->id);
    }
}