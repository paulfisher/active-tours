<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class TourController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        return view('profile.tours.index', compact('user'));
    }

    public function create() {
        $user = Auth::user();
        return view('profile.tours.create', compact('user'));
    }

    public function postCreate(TourRequest $tourRequest) {
        /** @var User $user */
        $user = Auth::user();
        $tour = $user->tours()->create($tourRequest->all());
        return redirect()->route('profile.tours.edit', $tour->id);
    }

    public function edit(Tour $tour) {
        $user = Auth::user();
        return view('profile.tours.edit', compact('user', 'tour'));
    }

    public function update(Tour $tour, TourRequest $tourRequest) {
        $tour->update($tourRequest->all());
        return redirect()->route('profile.tours');
    }

    public function toggleIsHidden(Tour $tour) {
        $tour->update([
            'is_hidden' => !$tour->is_hidden
        ]);
        return redirect()->back();
    }
}