<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class ProfileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        return view('profile.index', compact('user'));
    }

    public function toggleIsSeller() {
        /** @var User $user */
        $user = Auth::user();
        $user->update([
            'is_seller' => !$user->is_seller
        ]);
        return redirect()->back();
    }

    public function toggleSmsEnabled() {
        /** @var User $user */
        $user = Auth::user();
        $user->update([
            'is_sms_enabled' => !$user->is_sms_enabled
        ]);
        return redirect()->back();
    }

    public function confirmPhone() {
        /** @var User $user */
        $user = Auth::user();
        return view('profile.confirm_phone');
    }
}