<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function uploadTourImage(Tour $tour, ImageService $imageService) {
        if (Input::hasFile('image') && Input::file('image')->isValid()) {
            $image = Input::file('image');
            $imageService->storeImagesForTour($image, $tour);
        }
        return redirect()->route('profile.tours.edit', $tour->id);
    }

    public function deleteTourImage(TourImage $tourImage, ImageService $imageService) {
        $tourImage->delete();
        return redirect()->back();
    }
}