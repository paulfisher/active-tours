<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TourRequest;
use App\Http\Requests\TripRequest;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\TourImage;
use App\Models\Trip;
use App\Models\User;
use App\Services\BookingService;
use App\Services\ImageService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;


class SaleController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = Auth::user();
        $sales = $user->sales;
        return view('profile.sales.index', compact('user', 'sales'));
    }

    public function show(Booking $booking) {
        $user = Auth::user();
        return view('profile.sales.show', compact('booking', 'user'));
    }

    public function confirm(Booking $booking, BookingService $bookingService) {
        $bookingService->confirm($booking);
        $previous = URL::previous();
        if (!empty($previous)) {
            return redirect()->back();
        } else {
            return redirect()->route('profile.sales.show', $booking->id);
        }
    }

    public function cancel(Booking $booking, BookingService $bookingService) {
        $bookingService->cancel($booking);
        $previous = URL::previous();
        if (!empty($previous)) {
            return redirect()->back();
        } else {
            return redirect()->route('profile.sales.show', $booking->id);
        }
    }
}