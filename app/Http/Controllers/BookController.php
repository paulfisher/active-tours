<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookRequest;
use App\Models\Area;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\Trip;
use App\Models\User;
use App\Services\BookingService;
use App\Services\UserService;
use App\Services\Walletone\WalletoneProvider;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;


class BookController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['only' => 'pay']);
    }

    public function pay(Booking $booking, BookingService $bookingService) {
        if ($booking->payment_value == 0) {
            $bookingService->paid($booking, 0);
            return redirect()->route('profile.bookings.show', $booking->id);
        } else {
            return redirect()->route('profile.bookings.show', $booking->id)
                ->with('flash_error', 'К сожалению мы, разработчики, еще не интегрировали оплату на сайте. :(');
        }
    }

    public function tours(User $user) {
        $tours = $user->tours()->active()->get();
        $ownerId = $user->id;
        return view('book.form.tours', compact('user', 'tours', 'ownerId'));
    }

    public function information(User $user, Tour $tour, Trip $trip) {
        $ownerId = $tour->user->id;
        $name = '';
        $email = '';
        $phoneNumber = '';
        if (Auth::check()) {
            /** @var User $currentUser */
            $currentUser = Auth::user();
            $name = $currentUser->name;
            $email = $currentUser->email;
            $phoneNumber = $currentUser->phone_number;
        }
        return view('book.form.information', compact('tour', 'trip', 'name', 'email', 'phoneNumber', 'ownerId'));
    }

    public function confirmation(Booking $booking) {
        $ownerId = $booking->tourUser->id;
        return view('book.form.confirmation', compact('booking', 'ownerId'));
    }

    public function create(CreateBookRequest $createBookRequest, BookingService $bookingService, UserService $userService) {
        $trip = Trip::findOrFail($createBookRequest->trip_id);

        $user = Auth::user();
        if (!$user) {
            $findByEmail = User::where('email', $createBookRequest->email)->first();
            if ($findByEmail) {
                //надо залогинить его
                //abort(500, "Not implemented");
                //TODO: показывать форму логина!
                $user = $findByEmail;
            } else {
                $findByPhone = User::where('phone_number', $createBookRequest->phone_number)->first();
                if ($findByPhone) {
                    //надо залогинить его
                    //abort(500, "Not implemented");
                    //TODO: показывать форму логина!
                    $user = $findByPhone;
                } else {
                    $pwd = str_random(6);
                    $user = $userService->createOnBooking($createBookRequest, $pwd);
                    $this->notifyUserAccountCreated($user, $pwd);
                }
            }
        }
        if (!$user) {
            abort(500, 'user not found');
        }

        Auth::login($user);
        $booking = $bookingService->createBook(
            $user,
            $trip,
            $createBookRequest->members,
            $createBookRequest->description);

        return redirect()->action('BookController@confirmation', $booking->id);
    }

    public function voucher(Booking $booking) {
        $trip = $booking->trip;
        $tour = $trip->tour;
        return view('book.form.voucher', compact('booking', 'tour', 'trip'));
    }

    private function notifyUserAccountCreated(User $user, $pwd) {
        try {
            Mail::send('book.email.account-created-to-user', compact('user', 'pwd'), function ($message) use ($user) {
                $message->from('noreply@wiild.ru', 'Wiild.ru');
                $message->to($user->email)->subject('Ваш доступ на wiild.ru');
            });
        } catch (Exception $e) {}
    }
}
