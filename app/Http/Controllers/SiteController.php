<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use App\Models\Trip;
use App\Models\TripResponse;

class SiteController extends Controller {

    public function index() {
        $tours = Tour::active()->latest()->limit(3)->get();
        $responses = TripResponse::limit(3)->get();

        return view('index', compact('tours', 'responses'));
    }

    public function about() {
        return view('about');
    }

}