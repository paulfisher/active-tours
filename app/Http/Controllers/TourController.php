<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookRequest;
use App\Models\Area;
use App\Models\Booking;
use App\Models\Tour;
use App\Models\Trip;
use App\Models\User;
use App\Services\BookingService;
use Illuminate\Http\Request;

use App\Http\Requests;


class TourController extends Controller {

    public function bookTrip(Area $area, Tour $tour, Trip $trip) {
        $bookingFormSrc = route('book.information', [$tour->user->id, $tour->id, $trip->id]);
        $backSrc = route('tour.showTourTrip', [$area->slug, $tour, $trip]);
        return view('tour.book', compact('tour', 'trip', 'bookingFormSrc', 'backSrc'));
    }

    public function index(Request $request) {
        $area = $request->input('area');
        $category = $request->input('category');

        $tours = Tour::active()->limit(50);

        if (!empty($area)) {
            $tours = $tours->where('area_id', $area);
        }
        if (!empty($category)) {
            $tours = $tours->where('category', $category);
        }
        $tours = $tours->get();

        return view('tour.index', compact('tours'));
    }

    public function create() {}

    public function store(Request $request) {}

    public function showTour(Area $area, Tour $tour) {
        $similar = Tour::active()->latest()->limit(3)->get();
        return view('tour.show', compact('tour', 'similar'));
    }

    public function showTourTrip(Area $area, Tour $tour, Trip $trip) {
        $similar = Tour::active()->latest()->limit(3)->get();
        return view('tour.show', compact('tour', 'trip', 'similar'));
    }

    public function edit($id) {}

    public function update(Request $request, $id) {}

    public function destroy($id) {}
}
