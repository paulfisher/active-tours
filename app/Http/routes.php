<?php

Route::auth();

Route::get('/', 'SiteController@index');
Route::get('about', ['as' => 'about', 'uses' => 'SiteController@about']);

Route::resource('area', 'AreaController');

Route::get('tours', ['as' => 'tour.index', 'uses' => 'TourController@index']);

Route::group(['prefix' => 'booking'], function () {
    Route::post('/', 'BookController@create');

    //выбор тура
    Route::get('/{user}/tours', ['as' => 'book.tours', 'uses' => 'BookController@tours']);

    //ввод контактных данных
    Route::get('/{user}/tours/{tour}/{trip}/book', ['as' => 'book.information', 'uses' => 'BookController@information']);

    //оплата
    Route::get('/{booking}/pay', ['as' => 'book.pay', 'uses' => 'BookController@pay']);

    //подтверждение
    Route::get('/{booking}/confirmation', ['as' => 'book.confirmation', 'uses' => 'BookController@confirmation']);

    Route::get('/{booking}/confirm', ['as' => 'book.confirm', 'uses' => 'BookController@confirm']);
    Route::get('/{booking}/cancel', ['as' => 'book.cancel', 'uses' => 'BookController@cancel']);
});





Route::group(['prefix' => 'profile'], function () {
    Route::get('/', ['as' => 'profile.index', 'uses' => 'Profile\ProfileController@index']);
    Route::get('toggleIsSeller', ['as' => 'profile.toggleIsSeller', 'uses' => 'Profile\ProfileController@toggleIsSeller']);
    Route::get('toggleSmsEnabled', ['as' => 'profile.toggleSmsEnabled', 'uses' => 'Profile\ProfileController@toggleSmsEnabled']);

    Route::group(['prefix' => 'phone'], function () {
        Route::get('/', ['as' => 'profile.phone.index', 'uses' => 'Profile\PhoneController@index']);
        Route::get('/confirm', ['as' => 'profile.phone.confirm', 'uses' => 'Profile\PhoneController@confirm']);
        Route::post('/confirm', ['as' => 'profile.phone.postConfirm', 'uses' => 'Profile\PhoneController@postConfirm']);
        Route::get('/newCode', ['as' => 'profile.phone.newCode', 'uses' => 'Profile\PhoneController@newCode']);
    });


    Route::group(['prefix' => 'sales'], function () {
        Route::get('/', ['as' => 'profile.sales', 'uses' => 'Profile\SaleController@index']);
        Route::get('/{booking}', ['as' => 'profile.sales.show', 'uses' => 'Profile\SaleController@show']);
        Route::get('/{booking}/confirm', ['as' => 'profile.sales.confirm', 'uses' => 'Profile\SaleController@confirm']);
        Route::get('/{booking}/cancel', ['as' => 'profile.sales.cancel', 'uses' => 'Profile\SaleController@cancel']);
    });


    Route::group(['prefix' => 'bookings'], function () {
        Route::get('/', ['as' => 'profile.bookings', 'uses' => 'Profile\BookingController@index']);
        Route::get('{booking}', ['as' => 'profile.bookings.show', 'uses' => 'Profile\BookingController@show']);
    });

    Route::group(['prefix' => 'tours'], function () {
        Route::get('/', ['as' => 'profile.tours', 'uses' => 'Profile\TourController@index']);


        Route::get('create', ['as' => 'profile.tours.create', 'uses' => 'Profile\TourController@create']);
        Route::post('create', ['as' => 'profile.tours.postCreate', 'uses' => 'Profile\TourController@postCreate']);

        Route::get('{tour}/edit', ['as' => 'profile.tours.edit', 'uses' => 'Profile\TourController@edit']);
        Route::patch('{tour}/edit', ['as' => 'profile.tours.update', 'uses' => 'Profile\TourController@update']);

        Route::get('{tour}/toggleIsHidden', ['as' => 'profile.tours.toggleIsHidden', 'uses' => 'Profile\TourController@toggleIsHidden']);


        //TRIPS:
        Route::get('{tour}/trip/create', ['as' => 'profile.tours.trip.create', 'uses' => 'Profile\TripController@create']);
        Route::post('{tour}/trip/create', ['as' => 'profile.tours.trip.postCreate', 'uses' => 'Profile\TripController@postCreate']);

        //редактирование trip
        Route::get('{tour}/trip/{trip}/edit', ['as' => 'profile.tours.trip.edit', 'uses' => 'Profile\TripController@edit']);
        Route::patch('{tour}/trip/{trip}/edit', ['as' => 'profile.tours.trip.update', 'uses' => 'Profile\TripController@update']);

        //создание tour_image
        Route::post('{tour}/image/create', ['as' => 'profile.tours.image.create', 'uses' => 'Profile\ImageController@uploadTourImage']);
        //удаление tour_image
        Route::get('image/{tour_image}/delete', ['as' => 'profile.tours.image.delete', 'uses' => 'Profile\ImageController@deleteTourImage']);
        //TRIPS end


        //TOUR days:
        Route::get('{tour}/day/create', ['as' => 'profile.tours.day.create', 'uses' => 'Profile\TourDayController@create']);
        Route::post('{tour}/day/create', ['as' => 'profile.tours.day.postCreate', 'uses' => 'Profile\TourDayController@postCreate']);

        //редактирование day
        Route::get('{tour}/day/{day}/edit', ['as' => 'profile.tours.day.edit', 'uses' => 'Profile\TourDayController@edit']);
        Route::patch('{tour}/day/{day}/edit', ['as' => 'profile.tours.day.update', 'uses' => 'Profile\TourDayController@update']);

        Route::get('{tour}/day/{day}/delete', ['as' => 'profile.tours.day.delete', 'uses' => 'Profile\TourDayController@deleteDay']);
    });
});


Route::get('t/{area}/{tour}/{trip}', ['as' => 'tour.showTourTrip', 'uses' => 'TourController@showTourTrip']);
Route::get('t/{area}/{tour}', ['as' => 'tour.showTour', 'uses' => 'TourController@showTour']);
Route::get('t/{area}/{tour}/{trip}/book', ['as' => 'book.trip', 'uses' => 'TourController@bookTrip']);
