<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TourDayRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'tour_id' => 'required',
            'description' => 'required',
        ];
    }
}
