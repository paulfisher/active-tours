<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

/**
 * @property integer trip_id
 * @property integer members
 * @property string description
 * @property string email
 * @property string phone_number
 * @property string name
 * @property boolean is_sms_enabled
 */
class CreateBookRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required|digits:11',
            'trip_id' => 'required|integer',
            'members' => 'required|integer|max:255',
        ];
    }
}
