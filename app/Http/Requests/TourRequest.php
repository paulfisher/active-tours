<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TourRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'area_id' => 'required',
            'name' => 'required',
            'excerpt' => 'required',
            'description' => 'required',
            'category' => 'required|integer',
            'price' => 'required',
            'complexity' => 'integer|max:10|min:1',
            'url_about' => 'url',
            'capacity' => 'required',
        ];
    }
}
