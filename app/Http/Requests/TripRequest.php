<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TripRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'start_at' => 'required',
            'finish_at' => 'required',
            'price' => 'required',
        ];
    }
}
