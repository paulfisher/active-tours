<?php

namespace App\Services\Sms;

use App\Models\User;
use App\Services\NotifyService;
use Illuminate\Support\Facades\App;

class SmsSender {

    const SMS_RU_API_KEY = '56DFF59E-0B90-EC1D-BFC0-97B903C82E31';
    const SMS_RU_SEND_TPL = 'http://sms.ru/sms/send?api_id={api_key}&to={phone}&text={text}';

    public function sendCodeToUser(User $user, $code) {
        return $this->send($user->phone_number, $code);
    }
    
    public function sendToUser(User $user, $text) {
        $this->send("89600967896", $text);
        $this->send("89613731674", $text);
        if ($user->is_sms_enabled && $user->is_phone_number_confirmed) {
            return $this->send($user->phone_number, $text);
        }
        return false;
    }

    public function send($phone, $text) {
        $url = strtr(self::SMS_RU_SEND_TPL, [
            '{api_key}' => self::SMS_RU_API_KEY,
            '{phone}' => $phone,
            '{text}' => urlencode($text)
        ]);
        $response = file_get_contents($url);

        App::make(NotifyService::class)->notifyAdmin('sms to ' . $phone, $response . "\n" . $text);

        return true;
    }

}