<?php

namespace App\Services;


use App\Models\Booking;
use App\Models\Trip;
use App\Models\User;
use App\Services\Sms\SmsSender;
use Exception;
use Illuminate\Support\Facades\Mail;

class NotifyService {

    public function __construct(SmsSender $smsSender) {
        $this->smsSender = $smsSender;
    }

    public function notifyAdmin($title, $text) {
        try {
            Mail::raw($text, function ($message) use ($title) {
                $message->from('noreply@wiild.ru', 'Wiild.ru');
                $message->to('pavel.rybako@gmail.com')->subject($title);
            });
        } catch (Exception $e) {}
    }

    public function notifyOnBookCreated(Booking $booking, User $user, Trip $trip) {

        //email for tour organizer
        Mail::send('book.email.created-to-owner', compact('user', 'trip', 'booking'), function ($message) use ($trip) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to($trip->tour->user->email)->subject('Заявка на участие в туре. wiild.ru');
        });

        //email for tourist
        Mail::send('book.email.created-to-tourist', compact('user', 'trip', 'booking'), function ($message) use ($user, $trip) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to($user->email)->subject($trip->tour->name);
        });

        //email for admin
        Mail::send('book.email.created-to-admin', compact('user', 'trip', 'booking'), function ($message) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to('pavel.rybako@gmail.com')->subject('Новая заявка на wiild.ru');
        });

        $this->smsSender->sendToUser($trip->tour->user, 'Новая бронь на wiild.ru, подтвердите наличие места');
    }

    public function notifyOnBookConfirmed(Booking $booking) {
        //email for tourist
        Mail::send('book.email.confirmed-to-tourist', compact('booking'), function ($message) use ($booking) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to($booking->user->email)->subject('Поездка подтверждена');
        });

        $this->smsSender->sendToUser($booking->user, 'Ваше бронирование на wiild.ru подтверждено, можно перейти к оплате брони');
    }

    public function notifyOnBookCanceled(Booking $booking) {
        //email for tourist
        Mail::send('book.email.canceled-to-tourist', compact('booking'), function ($message) use ($booking) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to($booking->user->email)->subject('Поездка отменена');
        });

        $this->smsSender->sendToUser($booking->user, 'Организатор отменил ваше бронирование на wiild.ru');
    }

    public function notifyOnBookPaid(Booking $booking) {
        //email for organizer
        Mail::send('book.email.paid-to-owner', compact('booking'), function ($message) use ($booking) {
            $message->from('noreply@wiild.ru', 'Wiild.ru');
            $message->to($booking->tourUser->email)->subject('Поездка забронирована');
        });

        $this->smsSender->sendToUser($booking->tourUser, 'Оплачено. ' . $booking->user->name . ' ' . $booking->user->phone_number . '. Детали на wiild.ru');
    }

}