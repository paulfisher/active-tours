<?php

namespace App\Services;

use App\Models\Area;
use App\Models\Booking;
use App\Models\Trip;
use App\Models\User;
use App\Services\Sms\SmsSender;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class BookingService {

    const wiildCommission = 0;

    /** @var NotifyService */
    private $notifyService;

    public function __construct(NotifyService $notifyService) {
        $this->notifyService = $notifyService;
    }

    /**
     * @param User $user
     * @param Trip $trip
     * @param $members
     * @param null $description
     * @return Booking
     */
    public function createBook(User $user, Trip $trip, $members, $description = null) {

        $commission = self::wiildCommission;
        $booking = Booking::create([
            'user_id' => $user->id,
            'trip_id' => $trip->id,
            'tour_user_id' => $trip->tour->user->id,
            'status' => Booking::PENDING,
            'start_at' => $trip->start_at,
            'finish_at' => $trip->finish_at,
            'price' => $trip->price,
            'paid_value' => 0,
            'payment_value' => ($trip->price * $commission) / 100,
            'members' => $members,
            'description' => $description,
            'tour_name' => $trip->tour->name,
            'tour_excerpt' => $trip->tour->excerpt,
            'tour_description' => $trip->tour->description,
            'tour_category' => $trip->tour->category,
            'tour_price_includes' => $trip->tour->price_includes,
            'tour_price_excludes' => $trip->tour->price_excludes,
            'tour_complexity' => $trip->tour->complexity,
            'tour_url_about' => $trip->tour->url_about,
            'tour_capacity' => $trip->tour->capacity,
            'tour_area_name' => $trip->tour->area->name
        ]);

        $this->notifyService->notifyOnBookCreated($booking, $user, $trip);

        return $booking;
    }

    public function confirm(Booking $booking) {
        $booking->update([
            'status' => Booking::CONFIRMED
        ]);

        $this->notifyService->notifyOnBookConfirmed($booking);
    }

    public function cancel(Booking $booking) {
        $booking->update([
            'status' => Booking::CANCEL
        ]);

        $this->notifyService->notifyOnBookCanceled($booking);
    }

    public function paid(Booking $booking, $paidValue) {
        $booking->update([
            'status' => Booking::PAID,
            'paid_value' => $paidValue
        ]);

        $this->notifyService->notifyOnBookPaid($booking);
    }

}