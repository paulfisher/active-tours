<?php

namespace App\Services;

class CategoryService {

    public function getCategories() {
        return config('travel.categories');
    }

    public function getCategoriesList() {
        $categories = $this->getCategories();
        $result = [];
        foreach ($categories as $category) {
            $result[$category['id']] = $category['name'];
        }
        return $result;
    }

    public function getCategory($categoryId) {
        $categories = $this->getCategories();
        foreach ($categories as $category) {
            if ($category['id'] == $categoryId) {
                return $category;
            }
        }
        return null;
    }

}