<?php

namespace App\Services;

use App\Models\Tour;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ImageService {


    //куда кладем фотки
    private $imageDir = '/images/uploaded';

    const TOUR_SMALL_WIDTH = 300;
    const TOUR_MEDIUM_WIDTH = 600;
    const TOUR_LARGE_WIDTH = 1200;

    public function storeImagesForTour(UploadedFile $image, Tour $tour) {
        $dirToSave = $this->imageDir . '/' . $tour->id;

        $imageSrc = $dirToSave . '/' . $this->storeImage($image, $dirToSave);
        $smallSrc = $dirToSave . '/' . $this->resizeImage($imageSrc, self::TOUR_SMALL_WIDTH);
        $mediumSrc = $dirToSave . '/' . $this->resizeImage($imageSrc, self::TOUR_MEDIUM_WIDTH);
        $largeSrc= $dirToSave . '/' . $this->resizeImage($imageSrc, self::TOUR_LARGE_WIDTH);

        return $tour->images()->create([
            'src' => $imageSrc,
            'small_src' => $smallSrc,
            'medium_src' => $mediumSrc,
            'large_src' => $largeSrc,
        ]);
    }

    public function storeImage(UploadedFile $image, $dirToSave) {
        //генерим название для файла
        $extension = pathinfo($image->getClientOriginalName(), PATHINFO_EXTENSION);
        $baseName = pathinfo($image->getClientOriginalName(), PATHINFO_BASENAME);
        $baseName = substr(md5(time()), 0, 5) . '-' . str_replace('.' . $extension, '', $baseName);

        //добавляем extension
        $fileName = $baseName . '.' . $image->getClientOriginalExtension();

        //кладем файл-оригинал в папку
        $image->move(public_path() . $dirToSave, $fileName);

        return $fileName;
    }

    public function resizeImage($imageSrc, $width) {
        //получаем путь к файлу
        $filePath = public_path() . $imageSrc;

        $fileName = pathinfo($filePath, PATHINFO_BASENAME);
        $dirPath = pathinfo($filePath, PATHINFO_DIRNAME);

        //создаем новый экземпляр Image для ресайза
        /** @var \Intervention\Image\Image $img */
        $img = Image::make($filePath);

        //ресайзим
        if ($img->getWidth() > $width) {
            $img->widen($width);
        }

        //генерим название для файла
        $resizedFileName = '[w_' . $width . ']' . $fileName;

        //сохраняем файл
        $img->save($dirPath . '/' . $resizedFileName);

        //возвращаем путь к файлу
        return $resizedFileName;
    }
}