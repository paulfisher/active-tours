<?php

namespace App\Services;

use App\Http\Requests\CreateBookRequest;
use App\Models\User;
use App\Services\Sms\SmsSender;
use Illuminate\Support\Facades\App;

class UserService {

    /**
     * @var NotifyService
     */
    private $notifyService;

    public function __construct(NotifyService $notifyService) {
        $this->notifyService = $notifyService;
    }


    /**
     * @param CreateBookRequest $createBookRequest
     * @param $password
     * @return User
     */
    public function createOnBooking(CreateBookRequest $createBookRequest, $password) {
        $user = User::create([
            'name' => $createBookRequest->name,
            'email' => $createBookRequest->email,
            'password' => bcrypt($password),
            'phone_number' => $createBookRequest->phone_number,
            'is_sms_enabled' => !!$createBookRequest->is_sms_enabled,
            'is_phone_number_confirmed' => !!$createBookRequest->is_sms_enabled,
            'phone_number_confirm_code' => User::generatePhoneNumberConfirmCode()
        ]);

        $this->notifyService->notifyAdmin('Бронирование Wiild.ru', $user->name . ' ' . $user->id);

        return $user;
    }

    /**
     * @param array $data
     * @return User
     */
    public function createOnRegister(array $data) {
        $isSmsEnabled = !!$data['is_sms_enabled'];
        /** @var User $user */
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'phone_number' => $data['phone_number'],
            'is_sms_enabled' => $isSmsEnabled,
            'is_phone_number_confirmed' => false,
            'phone_number_confirm_code' => $isSmsEnabled ? User::generatePhoneNumberConfirmCode() : ''
        ]);

        if ($isSmsEnabled) {
            App::make(SmsSender::class)->sendCodeToUser($user, $user->phone_number_confirm_code);
        }

        $message = implode(' ', [
            'Регистрация:',
            $user->name,
            $user->email,
            $user->phone_number,
            '#' . $user->id
        ]);
        $this->notifyService->notifyAdmin('Регистрация на Wiild.ru', $message);

        return $user;
    }

}