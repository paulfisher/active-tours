<?php

namespace App\Services;

use App\Models\Area;

class AreaService {

    public function getAreas() {
        return Area::all();
    }

}