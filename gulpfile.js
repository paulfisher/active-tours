var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix
        .less('datepicker.less', 'resources/assets/css/datepicker/datepicker.css')

        .sass([
            'app.scss',
            'navigation.scss',
            'trip-list.scss',
            'mdi.scss'],
        'public/compiled/app.css')

        .styles([
            'js/ekko-lightbox/4.0.1/ekko-lightbox.min.css',
            'css/datepicker/datepicker.css'
        ],
        'public/compiled/plugins.css', 'resources/assets')

        .sass('main.scss', 'public/compiled/main.css')
        .sass('trip-show.scss', 'public/compiled/trip-show.css')

	    .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/**', 'public/build/fonts/bootstrap')
        .copy('node_modules/mdi/fonts/**', 'public/fonts/mdi')

        .browserify(['app.js', 'ekko-lightbox/4.0.1/ekko-lightbox.min.js'], 'public/compiled/app.js')

        .version([
            'public/compiled/plugins.css',
            'public/compiled/trip-show.css',
            'public/compiled/main.css',
            'public/compiled/app.css',
            'public/compiled/app.js'
        ]);

});
