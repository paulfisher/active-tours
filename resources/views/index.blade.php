@extends('layouts.app')

@push('head')
    <link href="{{ elixir('compiled/main.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

    <?php
        $splashImage = Illuminate\Support\Collection::make([
            '/images/rafting-679719.jpg',
            '/images/happy-woman-1452389.jpg',
            '/images/nature-1283693.jpg',
            '/images/ski-mountaineering-1375016.jpg',
        ])->random();
    ?>

    <style>
        .splash {
            position: relative;
            background: #141d27 url({{$splashImage}}) no-repeat left center;
            -o-background-size: cover;
            -webkit-background-size: cover;
            color: #fff;
            text-align: center;
            height: 500px;
        }
        .splash .header-container {
            position: absolute;
            top: 200px;
            width: 100%;
        }
        .splash h1, .splash p {
            text-shadow: 0 3px 3px rgba(0, 0, 0, 0.8);
        }
        .splash p {
            font-size: 18px;
        }
    </style>
    <div class="splash">
        @include('partials.search-panel')
        <div class="header-container text-center">
            <p>Заказывайте туры напрямую у организаторов путешествий</p>
            <a href="{{route('about')}}" class="btn btn-default" style="margin-top: 12px;">Как это работает</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @include('partials.areas')

                <hr/>

                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>Актуальные туры</h2>
                    </div>
                </div>
                @foreach($tours as $tour)
                    @include('tour.list-item', ['tour' => $tour])
                    <hr/>
                @endforeach

                <div class="row">
                    <div class="col-md-12">
                        <a href="{{route('tour.index')}}" class="btn btn-lg btn-default">Смотреть все туры</a>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- /container -->

@endsection