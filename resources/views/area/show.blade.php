@extends('layouts.app')

@section('content')

    <div class="container">
        {{--<div class="row">
            <div class="col-md-12">
                <h1>{{$area->name}}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>
                    {{$area->about}}
                </p>
            </div>
        </div>
        <hr/>--}}
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2>Туры {{$area->direction_form}}</h2>
                </div>
                @foreach($area->tours()->active()->get() as $tour)
                    @include('tour.list-item', ['tour' => $tour])
                    <hr/>
                @endforeach
            </div>
        </div>
    </div> <!-- /container -->

@endsection