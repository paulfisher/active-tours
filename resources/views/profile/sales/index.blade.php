@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'sales'])

                <hr/>

                <h4>Продажи</h4>


                <table class="table table-hover">
                    <tr>
                        <th>Тур</th>
                        <th>Участников</th>
                        <th></th>
                    </tr>
                    @foreach($sales as $booking)
                        <tr>
                            <td>
                                <a href="{{route('profile.sales.show', $booking->id)}}">
                                    {{$booking->tour_name}}
                                </a>
                            </td>
                            <td>
                                {{$booking->members}}
                            </td>
                            <td>
                                {{$booking->status_name}}
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection