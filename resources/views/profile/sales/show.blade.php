@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'sales'])

                <hr/>
                <a href="{{route('profile.sales')}}">&larr; назад</a>
                <h4>Заказ #{{$booking->id}}</h4>

                <div class="well">
                    @if ($booking->status == \App\Models\Booking::PAID)
                        <div class="alert alert-success">Бронирование подтверждено и оплачено</div>
                        <p>
                            <strong>Клиент</strong>
                        </p>
                        <p>
                            Имя: {{$booking->user->name}}<br/>
                            email: {{$booking->user->email}}<br/>
                            телефон: {{$booking->user->phone_number}}
                        </p>
                    @elseif ($booking->status == \App\Models\Booking::PENDING)
                        <a href="{{route('profile.sales.confirm', $booking->id)}}" class="btn btn-success">подтвердить</a>
                        <a href="{{route('profile.sales.cancel', $booking->id)}}" class="btn btn-link">отменить</a>
                    @elseif ($booking->status == \App\Models\Booking::CONFIRMED)
                        <div class="alert alert-info">Ожидается оплата от клиента</div>
                    @elseif ($booking->status == \App\Models\Booking::CANCEL)
                        <div class="alert alert-danger">Бронирование отменено</div>
                    @endif
                    <hr/>

                    <p>
                        <strong>
                            @if ($booking->trip)
                            <a href="{{route('profile.tours.edit', $booking->trip->tour->id)}}">
                                {{$booking->tour_name}}
                            </a>
                            @else
                                {{$booking->tour_name}}
                            @endif
                        </strong>
                        <span>{{$booking->tour_category_name}}</span>
                        <span>{{$booking->tour_area_name}}</span>
                    </p>
                    <p>
                        Даты поездки:
                        {{$booking->start_at->formatLocalized('%d %B, %a')}}
                        &mdash;
                        {{$booking->finish_at->formatLocalized('%d %B, %a')}}
                    </p>
                    <p>
                        Дата создания брони: {{$booking->created_at->formatLocalized('%d %B, %a')}}
                    </p>
                    <p>
                        Стоимость тура: {{$booking->price}} руб.
                    </p>
                    <p>
                        Участников: {{$booking->members}}
                    </p>
                    @if ($booking->description)
                    <p>
                        Пожелание: {{$booking->description}}
                    </p>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection