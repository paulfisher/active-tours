@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'home'])

                <hr/>

                <p>
                    e-mail: {{$user->email}}
                </p>
                <p>
                    номер телефона: {{link_to_route('profile.phone.index', $user->phone_number)}}
                </p>
                <hr/>
                @if (!$user->is_seller)
                    <a href="{{route('profile.toggleIsSeller')}}" class="btn btn-link btn-xs">включить продажи туров</a>
                @else
                    <p>Продажа туров включена.</p>
                    <a href="{{route('profile.toggleIsSeller')}}" class="btn btn-link btn-xs">отключить продажи туров</a>
                @endif
            </div>
        </div>
    </div>

@endsection