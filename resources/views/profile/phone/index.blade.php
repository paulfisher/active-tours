@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'home'])

                <hr/>

                <a href="{{route('profile.index')}}">&larr; назад</a>

                <h4>Мой номер телефона</h4>
                <b>{{$user->phone_number}}</b>

                <hr/>
                @if (!$user->is_sms_enabled)
                    <a href="{{route('profile.toggleSmsEnabled')}}" class="btn btn-link btn-xs">включить sms-оповещение</a>
                @else
                    @if (!$user->is_phone_number_confirmed)
                        <div class="alert alert-warning">необходимо {{link_to_route('profile.phone.confirm', 'подтвердить номер телефона')}}</div>
                    @endif

                    <p>Sms-уведомления включены.</p>
                    <a href="{{route('profile.toggleSmsEnabled')}}" class="btn btn-link btn-xs">отключить sms-оповещение</a>
                @endif
            </div>
        </div>
    </div>

@endsection