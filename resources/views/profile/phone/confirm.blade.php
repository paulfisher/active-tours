@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'home'])

                <hr/>

                <a href="{{route('profile.phone.index')}}">&larr; назад</a>


                @if ($user->is_sms_enabled && !$user->is_phone_number_confirmed)
                    @if ($user->phone_number_confirm_code)
                        <h4>Подтверждение номера телефона</h4>

                        {!! Form::open(['action' => ['Profile\PhoneController@postConfirm'], 'method' => 'POST']) !!}
                        <div class="form-inline">
                            <div class="form-group">
                                {!! Form::text('code', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Подтвердить', ['class' => 'btn btn-default']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    @endif
                        <a href="{{route('profile.phone.newCode')}}" class="btn btn-link">получить новый код</a>
                @endif
                @if ($user->is_sms_enabled && $user->is_phone_number_confirmed)
                    <div class="alert alert-success">Номер телефона {{$user->phone_number}} подтвержден</div>
                @endif


            </div>
        </div>
    </div>

@endsection