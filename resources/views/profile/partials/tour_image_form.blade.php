{!! Form::open(['action' => ['Profile\ImageController@uploadTourImage', $tour->id], 'files' => true]) !!}
<div class="form-inline">
    <div class="form-group">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Загрузить', ['class' => 'btn btn-default']) !!}
    </div>
</div>
{!! Form::close() !!}