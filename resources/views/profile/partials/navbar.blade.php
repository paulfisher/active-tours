
@if ($user->is_seller)
    <a href="{{route('profile.sales')}}" class="btn btn-default @if ($state == 'sales') active @endif">Продажи</a>
    <a href="{{route('profile.tours')}}" class="btn btn-default @if ($state == 'tours') active @endif">Управление турами</a>
@else
    <a href="{{route('profile.bookings')}}" class="btn btn-default @if ($state == 'bookings') active @endif">Мои заказы</a>
@endif
    <a href="{{route('profile.index')}}" class="btn btn-default @if ($state == 'home') active @endif">Настройки</a>