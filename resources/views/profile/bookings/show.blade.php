@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'bookings'])

                <hr/>
                <a href="{{route('profile.bookings')}}">&larr; назад</a>
                <h4>Заказ #{{$booking->id}}</h4>

                <div class="well">

                    @include('book.partials.voucher', ['booking' => $booking])
                </div>
            </div>
        </div>
    </div>

@endsection