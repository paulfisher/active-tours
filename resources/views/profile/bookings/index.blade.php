@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'bookings'])

                <hr/>

                <h4>Мои заказы</h4>

                <table class="table">
                    <tr>
                        <th>Поездка</th>
                        <th>Статус</th>
                        <th>Даты</th>
                        <th>Участников</th>
                        <th>&nbsp;</th>
                    </tr>
                @foreach($user->bookings as $booking)
                    <tr>
                        <td>
                            {{$booking->tour_name}}
                        </td>
                        <td>
                            {{$booking->status_name}}
                        </td>
                        <td>
                            {{$booking->start_at->formatLocalized('%d %B, %a')}}
                            &mdash;
                            {{$booking->finish_at->formatLocalized('%d %B, %a')}}
                        </td>
                        <td>
                            {{$booking->members}}
                        </td>
                        <td>
                            <a href="{{route('profile.bookings.show', $booking->id)}}" class="btn btn-link">подробнее</a>
                        </td>
                    </tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection