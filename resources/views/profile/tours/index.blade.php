@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>

                <h4>Управление турами</h4>

                <a href="{{route('profile.tours.create')}}" class="btn btn-default pull-right" style="margin-bottom: 20px;">+ добавить тур</a>

                <table class="table table-hover">
                    <tr>
                        <th>Тур</th>
                        <th>Короткое описание</th>
                        <th>Бронирований</th>
                        <th>&nbsp;</th>
                    </tr>
                    @foreach($user->tours as $tour)
                        <tr class="@if ($tour->is_hidden) warning @endif">
                            <td>
                                <a href="{{route('profile.tours.edit', $tour->id)}}">{{$tour->name}}</a>
                            </td>
                            <td>
                                <small>{{$tour->excerpt}}</small>
                            </td>
                            <td>
                                {{$tour->bookings()->count()}}
                            </td>
                            <td>
                                @if ($tour->is_hidden)
                                    <small class="text-danger">Скрыт на сайте</small>
                                    <a href="{{route('profile.tours.toggleIsHidden', $tour->id)}}" class="btn btn-link">
                                        <small>показывать</small>
                                    </a>
                                @else
                                    <a href="{{route('profile.tours.toggleIsHidden', $tour->id)}}" class="btn btn-link">
                                        <small>скрыть</small>
                                    </a>
                                @endif

                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
    </div>

@endsection