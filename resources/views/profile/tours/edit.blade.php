@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>
                <a href="{{route('profile.tours')}}">&larr; назад</a>
                <a href="{{route('tour.showTour', [$tour->area->slug, $tour->id])}}" target="_blank" class="pull-right">открыть тур на сайте</a>

                <h4>Редактирование тура &laquo;{{$tour->name}}&raquo;</h4>

                <div class="row">
                    <div class="col-md-6">
                        <div class="well">
                            <h5>Описание тура</h5>

                            @if (count($errors) > 0)
                                <div class="alert alert-warning">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {!! Form::model($tour, ['method' => 'PATCH', 'files' => true, 'action' => ['Profile\TourController@update', $tour->id]]) !!}
                            @include('profile.tours.form', ['submitButtonText' => 'Обновить'])
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="well">
                            <h5>Поездки</h5>

                            <a href="{{route('profile.tours.trip.create', $tour->id)}}" class="btn btn-default pull-right" style="margin-bottom: 20px;">+ добавить поездку</a>

                            <table class="table">
                                <tr>
                                    <th>Даты</th>
                                    <th>Цена</th>
                                    <th>&nbsp;</th>
                                </tr>
                            @foreach($tour->trips as $trip)
                                <tr>
                                    <td>
                                        <a href="{{route('profile.tours.trip.edit', [$trip->tour->id, $trip->id])}}">
                                            <small>{{$trip->start_at->formatLocalized('%d %B')}} &mdash; {{$trip->finish_at->formatLocalized('%d %B')}}</small>
                                        </a>
                                    </td>
                                    <td>{{number_format($trip->price, 0)}} р.</td>
                                    <td>
                                        <a href="{{route('tour.showTourTrip', [$tour->area->slug, $tour->id, $trip->id])}}" target="_blank" class="pull-right"><small>ссылка</small></a>
                                    </td>
                                </tr>
                            @endforeach
                            </table>
                        </div>


                        <div class="well">
                            <h5>Дни</h5>

                            <a href="{{route('profile.tours.day.create', $tour->id)}}" class="btn btn-default pull-right" style="margin-bottom: 20px;">+ добавить день</a>

                            <table class="table">
                                <tr>
                                    <th>День</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                @foreach($tour->days as $dayKey => $tourDay)
                                    <tr>
                                        <td>
                                            День {{++$dayKey}}
                                        </td>
                                        <td>
                                            <a href="{{route('profile.tours.day.edit', [$tour->id, $tourDay->id])}}">редактировать</a>
                                        </td>
                                        <td>
                                            <a href="{{route('profile.tours.day.delete', [$tour->id, $tourDay->id])}}">Удалить</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <div class="well">
                            <h5>Изображения тура</h5>
                            <div class="list-group">
                            @foreach($tour->images as $tourImage)
                                <div class="list-group-item">
                                    <img src="{{$tourImage->small_src}}" class="img-responsive img-thumbnail"/>
                                    <a href="{{route('profile.tours.image.delete', $tourImage->id)}}" onclick="return confirm('вы уверены, что хотите удалить изображение?');" class="btn btn-warning btn-xs">
                                        удалить
                                    </a>
                                </div>
                            @endforeach
                            </div>

                            <hr/>
                            <h5>Добавить изображение</h5>
                            @include('profile.partials.tour_image_form', ['tour' => $tour])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection