@inject('categories', 'App\Services\CategoryService')

<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Заголовок:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'autofocus']) !!}
    @if ($errors->has('name'))<small class="text-danger">Укажите название тура</small>@endif
</div>

<div class="form-group @if ($errors->has('excerpt')) has-error @endif">
    {!! Form::label('excerpt', 'Короткое описание:') !!}
    {!! Form::textarea('excerpt', null, ['class' => 'form-control']) !!}
    @if ($errors->has('excerpt'))<small class="text-danger">Укажите короткое описание</small>@endif
</div>

<div class="form-group @if ($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Полное описание тура:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    @if ($errors->has('description'))<small class="text-danger">Заполните описание тура</small>@endif
</div>

<div class="form-group @if ($errors->has('category')) has-error @endif">
    {!! Form::label('category', 'Тип:') !!}
    {!! Form::select('category', $categories->getCategoriesList(), null, ['class' => 'form-control']) !!}
    @if ($errors->has('category'))<small class="text-danger">Укажите тип</small>@endif
</div>

<div class="form-group @if ($errors->has('area_id')) has-error @endif">
    {!! Form::label('area_id', 'Регион:') !!}
    {!! Form::select('area_id', App\Models\Area::lists('name', 'id'), null, ['class' => 'form-control']) !!}
    @if ($errors->has('area_id'))<small class="text-danger">Укажите регион</small>@endif
</div>


<div class="form-group @if ($errors->has('price')) has-error @endif">
    {!! Form::label('price', 'Стоимость:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
    @if ($errors->has('price'))<small class="text-danger">Укажите стоимость тура</small>@endif
</div>

<div class="form-group @if ($errors->has('price_includes')) has-error @endif">
    {!! Form::label('price_includes', 'Входит в стоимость путевки:') !!}
    {!! Form::textarea('price_includes', null, ['class' => 'form-control']) !!}
    @if ($errors->has('price_includes'))<small class="text-danger">Укажите, что входит в стоимость тура</small>@endif
</div>

<div class="form-group @if ($errors->has('price_excludes')) has-error @endif">
    {!! Form::label('price_excludes', 'Не входит в стоимость путевки:') !!}
    {!! Form::textarea('price_excludes', null, ['class' => 'form-control']) !!}
    @if ($errors->has('price_excludes'))<small class="text-danger">Укажите, что не входит в стоимость тура</small>@endif
</div>

<div class="form-group @if ($errors->has('complexity')) has-error @endif">
    {!! Form::label('complexity', 'Сложность(от 1 до 10):') !!}
    {!! Form::number('complexity', null, ['class' => 'form-control']) !!}
    @if ($errors->has('complexity'))<small class="text-danger">Оцените сложность тура от 1 до 10 включительно</small>@endif
</div>

<div class="form-group @if ($errors->has('url_about')) has-error @endif">
    {!! Form::label('url_about', 'Ссылка:') !!}
    {!! Form::text('url_about', null, ['class' => 'form-control']) !!}
    @if ($errors->has('url_about'))<small class="text-danger">Укажите корректный урл адрес тура</small>@endif
</div>

<div class="form-group @if ($errors->has('capacity')) has-error @endif">
    {!! Form::label('capacity', 'Информация о размере группы:') !!}
    {!! Form::textarea('capacity', null, ['class' => 'form-control']) !!}
    @if ($errors->has('capacity'))<small class="text-danger">Укажите примерный размер группы</small>@endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}
</div>
