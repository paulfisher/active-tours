@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>
                <a href="{{route('profile.tours.edit', $tour->id)}}">&larr; назад</a>

                <h4>Редактирование поездки {{$trip->id}} &laquo;{{$tour->name}}&raquo;</h4>

                <div class="well">
                    {!! Form::model($trip, ['method' => 'PATCH', 'files' => true, 'action' => ['Profile\TripController@update', $tour->id, $trip->id]]) !!}
                    @include('profile.tours.trip.form', ['submitButtonText' => 'Обновить'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection