<div class="form-group @if ($errors->has('start_at')) has-error @endif">
    {!! Form::label('start_at', 'Дата начала:') !!}
    {!! Form::date('start_at', $trip->start_at, ['class' => 'form-control', 'autofocus']) !!}
    @if ($errors->has('start_at'))<small class="text-danger">Укажите дату начала поездки</small>@endif
</div>

<div class="form-group @if ($errors->has('finish_at')) has-error @endif">
    {!! Form::label('finish_at', 'Дата окончания:') !!}
    {!! Form::date('finish_at', $trip->finish_at, ['class' => 'form-control']) !!}
    @if ($errors->has('finish_at'))<small class="text-danger">Укажите дату окончания поездки</small>@endif
</div>



<div class="form-group @if ($errors->has('price')) has-error @endif">
    {!! Form::label('price', 'Стоимость:') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
    @if ($errors->has('price'))<small class="text-danger">Укажите стоимость тура</small>@endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}
</div>
