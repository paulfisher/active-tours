

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>
                <a href="{{route('profile.tours')}}">&larr; назад</a>

                <h4>Новый тур</h4>

                <div class="well">

                    {!! Form::model($tour = new \App\Models\Tour, ['action' => ['Profile\TourController@postCreate'], 'files' => true]) !!}
                    @include('profile.tours.form', ['submitButtonText' => 'Сохранить'])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection