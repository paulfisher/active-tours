@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>
                <a href="{{route('profile.tours.edit', $tour->id)}}">&larr; назад</a>

                <h4>Редактирование дня {{$tourDay->id}} &laquo;{{$tour->name}}&raquo;</h4>

                <div class="well">
                    {!! Form::model($tourDay, ['method' => 'PATCH', 'files' => true, 'action' => ['Profile\TourDayController@update', $tour->id, $tourDay->id]]) !!}
                    @include('profile.tours.day.form', ['submitButtonText' => 'Обновить'])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection