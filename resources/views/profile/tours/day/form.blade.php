{!! Form::hidden('tour_id', $tour->id) !!}


<div class="form-group @if ($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Описание:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
    @if ($errors->has('description'))<small class="text-danger">Укажите описание</small>@endif
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-default']) !!}
</div>
