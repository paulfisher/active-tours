

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>{{$user->name}}</h3>

                @include('profile.partials.navbar', ['state' => 'tours'])

                <hr/>
                <a href="{{route('profile.tours.edit', $tour->id)}}">&larr; назад</a>

                <h4>Новый день для тура &laquo;{{$tour->name}}&raquo;</h4>

                <div class="well">

                    {!! Form::model($tourDay = new \App\Models\TourDay, ['action' => ['Profile\TourDayController@postCreate', $tour->id], 'files' => true]) !!}
                    @include('profile.tours.day.form', ['submitButtonText' => 'Сохранить'])
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>

@endsection