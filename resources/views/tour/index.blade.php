@extends('layouts.app')

@section('content')
    <style>
        .search-form-container {
            position: relative;
            padding-top: 20px;
            padding-bottom: 20px;
            background: rgba(0, 0, 0, 0.3);

        }
        .search-form-image {
            background-size: cover;
            background-image: url(/images/search1.jpg);
        }
    </style>
    <div class="search-form-image">
        @include('partials.search-panel')
    </div>
    <div class="container">
        <div class="row" style="margin-top:16px;">
            <div class="col-md-12">
                @if(isset($tours))
                    @foreach($tours as $tour)
                        @include('tour.list-item', [
                             'tour' => $tour,
                         ])
                        <hr/>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-md-12">
                            Туры не найдены
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div> <!-- /container -->
@endsection