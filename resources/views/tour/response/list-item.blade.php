<div class="list-group-item-text media">
    <div class="media-left media-middle">
        <a href="#">
            <img src="{{$response->image_src}}" class="media-object"/>
        </a>
    </div>
    <div class="media-body">
        <h4 class="media-heading">{{$response->title}}</h4>


        <div>
            <?php
                $starCounter = 2;
                $mark = $rating / 2;
            ?>

            <span class="text-muted" style="font-size: 12px;">
                @while($starCounter <= 10)
                    @if ($rating >= $starCounter)
                        <i class="mdi mdi-star mdi-18px" style="color: #ffc101;"></i>
                    @elseif ($rating+1 == $starCounter)
                        <i class="mdi mdi-star-half mdi-18px" style="color: #ffc101;"></i>
                    @else
                        <i class="mdi mdi-star-outline mdi-18px" style="color: #ffc101;"></i>
                    @endif
                    <?php $starCounter+=2; ?>
                @endwhile
                {{$mark}} - Отлично
            </span>

            <span class="pull-right text-muted" style="font-size: 12px;">
                Отзыв написан {{$response->created_at->formatLocalized('%B, %d')}}
            </span>
        </div>

        <p style="padding-top: 12px;">
            {{$response->content}}
        </p>
    </div>
</div>
