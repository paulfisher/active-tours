<div class="row">
    <div class="col-md-12">
        <ul class="list-group">
            @foreach($responses as $response)
                <li class="list-group-item">
                    @include('trip.response.list-item', ['response' => $response])
                </li>
            @endforeach
        </ul>
    </div>
</div>