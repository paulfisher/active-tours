@extends('layouts.app')

@push('head')
    <link href="{{ elixir('compiled/trip-show.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>
                    <small><a href="{{route('area.show', $tour->area->slug)}}">{{$tour->area->name}}</a> / </small>
                    {{$tour->name}}
                </h2>
                @if (isset($trip))
                    <p>
                        <span class="tour-info-dates">
                            {{$trip->start_at->formatLocalized('%d %B, %a')}} - {{$trip->finish_at->formatLocalized('%d %B, %a')}}
                        </span>
                    </p>
                @endif
                <p>
                    {{$tour->excerpt}}
                </p>
            </div>
        </div>

        @if ($tour->images()->exists())
            <div class="row">
                <div class="col-md-6">
                    @include('tour.partials.tour-carousel')
                </div>
                <div class="col-md-6">
                    @include('tour.partials.tour-info')
                </div>
            </div>
            <hr/>

            <div class="row">
                <div class="col-md-12">
                    <p>{!! $tour->description !!}</p>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    @include('tour.partials.tour-info')
                </div>
                <div class="col-md-6">
                    <p>{!! $tour->description !!}</p>
                </div>
            </div>
        @endif

        @if ($tour->days()->exists())
        <hr/>
        @endif
        @foreach($tour->days as $dayKey => $day)
            <div class="row">
                <div class="col-md-2">
                    <h4>{{++$dayKey}} день</h4>
                </div>
                <div class="col-md-10">
                    <div class="well">
                        {!! $day->description !!}
                    </div>
                </div>
            </div>
        @endforeach


        @if (isset($trip) && $trip->responses()->exists())
        <hr id="testimonials"/>
        <div class="row">
            <div class="col-md-12 text-center">
                <h4>Отзывы</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    @foreach($trip->responses as $response)
                        <li class="list-group-item">
                            @include('tour.response.list-item', ['response' => $response])
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif

        @if ($tour->trips()->count() > 1 || $tour->trips()->count() > 0 && !isset($trip))
        <hr id="trips"/>
        <div class="row">
            <div class="col-md-12">
                <h5>Другие даты этого тура</h5>
                <div class="list-group">
                    @foreach($tour->trips as $anotherTrip)
                        @if (!isset($trip) || $anotherTrip->id != $trip->id)
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                {{$anotherTrip->start_at->formatLocalized('%d %B, %a')}} - {{$anotherTrip->finish_at->formatLocalized('%d %B, %a')}}
                            </div>
                            <div class="col-md-4">
                                {{$anotherTrip->price}} руб.
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-sm btn-warning"
                                   href="{{route('tour.showTourTrip', [$anotherTrip->tour->area->slug, $anotherTrip->tour->id, $anotherTrip->id])}}">
                                    Забронировать
                                </a>
                            </div>
                        </div>
                    </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        @endif
    </div> <!-- /container -->

@endsection

@push('footer_scripts')
<style>
    .o-tour-dates-price-container {
        overflow: hidden;
        background: #ffffff;
        z-index: 999;
        width: 555px;
    }
    .o-tour-dates-price-container.affix {
        top: 20px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
    }
    .o-tour-dates-price-container.affix-bottom {
        position: absolute;
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
    }
</style>
@endpush