<div class="panel panel-default" style="width: 100%;">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted">Организатор тура</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="//pp.vk.me/c630319/v630319404/25929/zCi1Oyj24w0.jpg" style="width:60px;"/>
            </div>
            <div class="col-md-8">
                <p style="font-weight: bold;">{{$tour->user->name}}</p>
                <p><a href="#tour-responses"><small>отзывы</small></a></p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <a href="#tour-qa" class="btn btn-default" style="width: 100%;">Задать вопрос</a>
            </div>
        </div>
    </div>
</div>