<div class="photo-container">
    @foreach($tour->images as $key => $image)
        <a href="{{$image->large_src}}"
           data-toggle="lightbox"
           data-gallery="trip-gallery"
           style="width:100px;height:100px;overflow:hidden;display: block;float:left;padding: 3px;">
            <img alt="" src="{{$image->small_src}}" style="height: 100px;"/>
        </a>
    @endforeach
</div>