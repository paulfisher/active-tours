<div class="panel panel-default tour-booking-widget">
    <div class="panel-body">
        <p style="font-size: 18px;">Заявка на бронирование</p>
        <div class="form">
            <div class="form-group">
                <label for="">Дата</label>
                <select class="form-control" name="">
                    <option value="">Выбрать дату поездки</option>
                    @foreach($tour->trips as $anotherTrip)
                        <option value="{{$anotherTrip->id}}" @if ($anotherTrip->id == $trip->id) selected="selected" @endif>
                            {{$anotherTrip->start_at->formatLocalized('%d %B, %a')}}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="">Имя</label>
                <input type="text" placeholder="Имя" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="">E-mail</label>
                <input type="text" placeholder="E-mail" class="form-control"/>
            </div>
            <div class="form-group">
                <label for="">Номер телефона</label>
                <input type="text" placeholder="Номер телефона" class="form-control"/>
            </div>
            <div class="form-group" style="margin-top: 24px;">
                <a class="btn btn-danger"
                   href="{{route('book.trip', [$trip->tour->area->slug, $trip->tour->id, $trip->id])}}">
                    Запросить бронирование
                </a>
            </div>
            <p class="text-muted" style="width: 228px;">
                <small class="text-muted"
                       data-toggle="popover"
                       data-placement="left"
                       data-content="Оплата производится организатору тура напрямую. Сайт wiild.ru предоставляет лишь информацию о турах и возможность запроса бронирования."
                       data-trigger="hover">
                    <i class="mdi mdi-help-circle mdi-24px mdi-dark mdi-inactive"></i>
                </small>
                <small>
                    Отправка заявки является предварительной и не обязует к оплате или бронированию
                </small>
            </p>
        </div>
    </div>
</div>


@push('footer_scripts')
<style>
    .tour-booking-widget.affix {
        top: 10px;
    }
</style>
<script>
    $(function() {
        var bookingWidget = $('.tour-booking-widget');
        var widgetTop = bookingWidget.offset().top;
        bookingWidget.affix({
            offset: {
                top: widgetTop-60,
                bottom: function () {
                    return (this.bottom = $('footer').outerHeight(true))
                }
            }
        });
    });
</script>
@endpush