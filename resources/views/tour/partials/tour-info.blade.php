<div class="row">
    <div class="col-md-6">
        <strong>Что включено в стоимость:</strong>
        <p>
            {!! nl2br($tour->price_includes) !!}
        </p>
    </div>
    <div class="col-md-6">
        <strong>Не входит в стоимость:</strong>
        <p>
            {!! nl2br($tour->price_excludes) !!}
        </p>
    </div>
</div>