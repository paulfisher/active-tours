<div class="well">
    <div class="row tour-item-container">
        <div class="col-md-3">
            @if ($tour->images()->exists())

                @if ($tour->trips()->exists())
                    <a href="{{route('tour.showTourTrip', [$tour->area->slug, $tour->id, $tour->trips()->first()->id])}}" title="{{$tour->name}}">
                        <img src="{{$tour->images->first()->small_src}}" class="img-responsive"/>
                    </a>
                @else
                    <a href="{{route('tour.showTour', [$tour->area->slug, $tour->id])}}" title="{{$tour->name}}">
                        <img src="{{$tour->images->first()->small_src}}" class="img-responsive"/>
                    </a>
                @endif
            @endif
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-9">
                    <p class="tour-title">
                        <a class="tour-region" href="{{route('area.show', $tour->area->slug)}}" title="{{$tour->area->name}}">{{$tour->area->name}}</a>

                        <br/>
                        @if ($tour->trips()->exists())
                            <a href="{{route('tour.showTourTrip', [$tour->area->slug, $tour->id, $tour->trips()->first()->id])}}" title="{{$tour->name}}" class="tour-title-name">{{$tour->name}}</a>
                        @else
                            <a href="{{route('tour.showTour', [$tour->area->slug, $tour->id])}}" title="{{$tour->name}}" class="tour-title-name">{{$tour->name}}</a>
                        @endif

                        <br/>

                        @if ($tour->trips()->exists())
                            <i class="mdi mdi-calendar-clock mdi-24px mdi-dark mdi-inactive"></i>
                            {{$tour->trips()->first()->start_at->formatLocalized('%B, %d')}},
                            {{max($tour->trips()->first()->start_at->diffInDays($tour->trips()->first()->finish_at), 1)}} дн.
                        @endif
                    </p>
                    <p class="tour-description">
                        {{$tour->excerpt}}
                    </p>
                </div>
                <div class="col-md-3">
                    <p>
                        <span class="tour-currency pull-right">руб</span>
                        <span class="tour-price pull-right">
                            @if ($tour->trips()->exists())
                                {{number_format($tour->trips()->first()->price, 0)}}
                            @else
                                {{number_format($tour->price, 0)}}
                            @endif
                        </span>

                        @if ($tour->trips()->exists())
                            <a href="{{route('tour.showTourTrip', [$tour->area->slug, $tour->id, $tour->trips()->first()->id])}}"
                               class="btn btn-success tour-book">
                                Подробнее
                            </a>
                        @else
                            <a href="{{route('tour.showTour', [$tour->area->slug, $tour->id])}}" class="btn btn-success tour-book">
                                Подробнее
                            </a>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>

</div>