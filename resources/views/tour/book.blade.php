@extends('layouts.empty')

@section('content')

    <div class="container" style="margin-top:22px;">
        <div class="row">
            <div class="col-md-12">
                <a href="{{$backSrc}}">&larr; назад</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <iframe src="{{$bookingFormSrc}}" width="1000" height="1000" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
    </div> <!-- /container -->
@endsection