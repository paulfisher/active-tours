@inject('areas', 'App\Services\AreaService')
@inject('categories', 'App\Services\CategoryService')

<footer style="margin-top: 40px;">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h5>О wiild.ru</h5>
                <p>
                    Найти путешествие по России, забронировать и отправиться в увлекательное приключение со своими друзьями
                </p>
                <p>
                    <a href="javascript: void 0;" style="width:26px;height:26px;display:block;background: #597DA3;color:#ffffff;font-weight: bold;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;font-size:16px;padding-top:2px;text-align: center;">
                        В
                    </a>
                </p>
            </div>
            <div class="col-md-4">
                <h5>Направления</h5>
                    @foreach($areas->getAreas() as $area)
                        <a href="{{route('area.show', $area->slug)}}">
                            <small>
                                {{$area->name}}
                            </small>
                        </a>
                    @endforeach
            </div>
            <div class="col-md-4">
                <h5>Туры</h5>
                @foreach($categories->getCategories() as $category )
                    <a href="{{route('tour.index', ['category' => $category['id']])}}">
                        <small>
                            {{$category['name']}}
                        </small>
                    </a>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>
                    &copy; <a href="/" title="Активные туры по России" class="text-muted">wiild.ru</a>, {{\Carbon\Carbon::now()->year}}
                </p>
            </div>
        </div>
    </div>
</footer>