<div class="row">
    <div class="col-md-12">
        <ul class="list-group">
            @foreach($responses as $response)
            <li class="list-group-item">
                <div class="list-group-item-text media">
                    <div class="media-left media-middle">
                        <a href="#">
                            <img src="{{$response->image_src}}" class="media-object"/>
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">{{$response->title}}</h4>
                        {{$response->content}}
                    </div>
                </div>
            </li>
            @endforeach
        </ul>
        <a href="javascript: void 0;" class="btn btn-lg btn-default">Читать все отзывы</a>
    </div>
</div>