@inject('categories', 'App\Services\CategoryService')
@inject('areas', 'App\Services\AreaService')

<div class="@if (Request::is('/')) navbar-wrapper-main @else navbar-wrapper @endif">
    <nav class="navbar @if (Request::is('/')) navbar-default @else navbar-inverse @endif navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-uppercase" href="/" style="font-size: 28px;">wiild.ru</a>
            </div>

            @if (!isset($empty))
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('about') }}">О нас</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Направления <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach($areas->getAreas() as $area)
                                <li>
                                    <a href="{{route('area.show', $area->slug)}}">{{$area->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Вход</a></li>
                        <li><a href="{{ url('/register') }}">Регистрация</a></li>
                        <li><a href="{{ url('/register') }}" class="btn">Стать организатором</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if (Auth::user()->is_seller)
                                    <li><a href="{{ route('profile.sales') }}">Продажи</a></li>
                                    <li><a href="{{ route('profile.tours') }}">Туры</a></li>
                                @endif
                                    <li><a href="{{ route('profile.index') }}">Мой профиль</a></li>
                                <li><hr/></li>
                                <li><a href="{{ url('/logout') }}">Выйти</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
            @endif {{--!isset($empty)--}}
        </div>
    </nav>
</div>