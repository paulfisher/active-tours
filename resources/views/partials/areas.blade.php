<style>
    .direction h2 {
        position: absolute;
        top: 50px;
        left: 0;
        width: 100%;
        color: #ffffff;
        font-weight: 600;
        text-shadow: 1px 2px 3px rgba(0,0,0,0.5);
    }
</style>

<div class="row" style="margin-top: 22px;margin-bottom: 18px;">
    <div class="col-md-12 text-center">
        <h3>Открывайте Россию</h3>
        <p>Посмотрите, куда отправляются путешественники со всей России</p>
    </div>
</div>

<div class="row">
    <div class="col-md-8 direction">
        <a href="{{route('area.show', 'altai')}}" title="Алтай">
            <h2 class="text-center">Алтай</h2>
            <img src="/images/altai.jpg" class="img-responsive" alt="Алтай"/>
        </a>
    </div>
    <div class="col-md-4 direction">
        <a href="{{route('area.show', 'karelia')}}" title="Карелия">
            <h2 class="text-center">Карелия</h2>
            <img src="/images/karelia1.jpg" class="img-responsive" alt="Карелия"/>
        </a>
    </div>
</div>

<div class="row" style="margin-top: 30px;">
    <div class="col-md-4 direction">
        <a href="{{route('area.show', 'ural')}}" title="Урал">
            <h2 class="text-center">Урал</h2>
            <img src="/images/ural.jpg" class="img-responsive" alt="Урал"/>
        </a>
    </div>
    <div class="col-md-4 direction">
        <a href="{{route('area.show', 'kavkaz')}}" title="Кавказ">
            <h2 class="text-center">Кавказ</h2>
            <img src="/images/kavkaz.jpg" class="img-responsive" alt="Кавказ"/>
        </a>
    </div>
    <div class="col-md-4 direction">
        <a href="{{route('area.show', 'kamchatka')}}" title="Камчатка">
            <h2 class="text-center">Камчатка</h2>
            <img src="/images/kamchatka.jpg" class="img-responsive" alt="Камчатка"/>
        </a>
    </div>
</div>