<div class="row tour-item-container">
    <div class="col-md-3">
        <img src="{{$img}}" class="img-responsive"/>
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-9">
                <p class="tour-title">
                    <span class="tour-region">Алтай,</span>
                    <span class="tour-dates">24 июня, Пт &rarr; 28 июня, Вт</span>
                    <br/>
                    {{$name}}
                </p>
                <p class="tour-description">
                    Отличная возможность провести короткий отпуск на реке Катунь. В программе:
                    сплав по реке Катунь с опытным инструктором, походная баня, рыбалка, экскурсии
                    в пещеры
                </p>
            </div>
            <div class="col-md-3">
                <span class="tour-currency pull-right">руб</span>
                <span class="tour-price pull-right">{{$price}}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p class="tour-places">
                    осталось 8 мест
                </p>
            </div>
            <div class="col-md-6 text-right">
                <a href="/tour" class="btn btn-link">Добавить в избранное</a>
                <a href="{{route('trip.show', $trip->id)}}" class="btn btn-success tour-book">Подробнее</a>
            </div>
        </div>
    </div>
</div>
