@inject('categories', 'App\Services\CategoryService')
@inject('areas', 'App\Services\AreaService')

<style>
    #input-area-dropdown, #input-category-dropdown {
        display: none;
    }
</style>
<div class="search-form-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <form class="form-inline" action="{{route('tour.index')}}">
                    <div class="form-group">
                        <input type="text" id="input-area" class="form-control" placeholder="Куда" readonly>
                        <input type="text" id="id-area" name="area" style="display: none">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="input-area-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                categories
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="dropdown-menu-area" aria-labelledby="input-area-dropdown">
                                @foreach( $areas->getAreas() as $area )
                                    <li>
                                        <a onclick = "selectArea('{{$area->name}}','{{$area->id}}')">{{$area->name}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    {{--<div class="form-group">
                        <input type="text" id="date-picker" name="date" class="form-control" placeholder="Когда" readonly>
                    </div>--}}
                    <div class="form-group">
                        <input type="text" id ="input-category" class="form-control" placeholder="Как" readonly>
                        <input type="text" id ="id-category" name="category" style="display: none">
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle" type="button" id="input-category-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                categories
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="dropdown-menu-category" aria-labelledby="input-category-dropdown">
                                @foreach( $categories->getCategories() as $category )
                                    <li>
                                        <a onclick = "selectCategory('{{$category['name']}}', '{{$category['id']}}')" >{{$category['name']}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-danger">Найти</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        $('#date-picker').datepicker({
            format: 'dd-mm-yyyy',
            startDate: '+1d',
            language: 'ru',
            autoclose: true
        });

        document.getElementById("input-area").onclick = function (e) {
            $('#input-area-dropdown').click();
            e.stopPropagation();
        };
        document.getElementById("input-category").onclick = function (e) {
            $('#input-category-dropdown').click();
            e.stopPropagation();
        };
    };

    function selectArea(area, id) {
        document.getElementById("input-area").value = area;
        document.getElementById("id-area").value = id;
    }
    function selectCategory(category, id) {
        document.getElementById("input-category").value = category;
        document.getElementById("id-category").value = id;
    }
</script>