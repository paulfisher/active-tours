@if ($booking->isPending())
    <div class="alert alert-warning">
        Внимание! Ваша поездка ожидает подтверждения от организатора путешествия.
        Как только организатор подтвердит наличие места для вас, на ваш email и телефон
        придёт сообщение с сылкой на оплату бронирования.
    </div>
@elseif ($booking->isConfirmed())
    <div class="alert alert-warning">
        Организатор зарезервировал место для вас в поездке. Вы можете перейти к оплате бронирования<br/>
        <a href="{{route('book.pay', $booking->id, true)}}">оплатить</a>
    </div>
@elseif ($booking->isCancel())
    <div class="alert alert-danger">
        К сожалению организатор отменил ваше бронирование.
    </div>
@elseif ($booking->isPaid())
    <div class="alert alert-success">
        Бронирование подтверждено и оплачено
    </div>
@endif

<h5>Подтверждение на путешествие &laquo;{{$booking->tour_name}}&raquo;</h5>

<p>
    <strong>Даты поездки:</strong> {{$booking->start_at->formatLocalized('%d %B, %a')}} &mdash; {{$booking->finish_at->formatLocalized('%d %B, %a')}}
</p>

<p>
    <strong>Оплачено:</strong> {{number_format($booking->paid_value, 0)}} руб.
</p>
<p>
    <strong>Оплатить организатору тура:</strong> {{number_format($booking->price - $booking->paid_value, 0)}} руб.
</p>

<p>
    <strong>Участников:</strong> {{$booking->members}}
</p>

@if ($booking->description)
    <p>
        <strong>Особые пожелания:</strong>
        {{$booking->description}}
    </p>
@endif

<p>
    Ваши заказы вы можете посмотреть <a href="{{route('profile.bookings', [], true)}}" target="_blank">в личном кабинете</a>
</p>