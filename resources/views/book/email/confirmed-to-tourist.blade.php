Привет {{$booking->user->name}}
<br/>
<br/>
Хорошие новости! Организатор тура <a href="{{route('tour.showTourTrip', [$booking->trip->tour->area->slug, $booking->trip->tour->id, $booking->trip->id], true)}}">{{$booking->trip->tour->name}}</a> подтвердил наличие места
<br/>
<br/>
<a href="{{route('book.pay', $booking->id, true)}}">Оплатить бронирование</a>
<br/>
<br/>
Команда wiild.ru
