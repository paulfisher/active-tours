Привет {{$trip->tour->user->name}}
<br/>
<br/>
Хорошие новости! На твой тур <a href="{{route('tour.showTourTrip', [$trip->tour->area->slug, $trip->tour->id, $trip->id], true)}}">{{$trip->tour->name}}</a> подал заявку {{$user->name}}
<br/>
<br/>
<a href="{{route('profile.sales.show', $booking->id)}}" style="font-size: 16px;text-decoration: none;color:#ffffff;background: #666666;padding:5px;border: 1px solid #222222;">
    Открыть бронирование в личном кабинете
</a>
<br/>
<br/>
Спасибо за сотрудничество. Команда wiild.ru
