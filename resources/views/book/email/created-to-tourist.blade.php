Привет {{$user->name}}
<br/>
<br/>
Хорошие новости! Организатор тура <a href="{{route('tour.showTourTrip', [$trip->tour->area->slug, $trip->tour->id, $trip->id], true)}}">{{$trip->tour->name}}</a> получил твои контактные данные и свяжется с тобой в самое ближайшее время.
<br/>
<br/>
Команда wiild.ru
