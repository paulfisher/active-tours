@extends('layouts.book', ['step' => 'tours'])

@section('booking_content')

    <h5>Доступные туры</h5>

    @foreach($tours as $tour)
        @foreach($tour->trips as $trip)
        <p>
            <a href="{{route('book.information', [$user->id, $tour->id, $trip->id])}}">
                {{$tour->name}}
            </a>
            {{$trip->start_at->formatLocalized('%d %B, %a')}} &mdash; {{$trip->finish_at->formatLocalized('%d %B, %a')}}
        </p>
        @endforeach
    @endforeach

@endsection