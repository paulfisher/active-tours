@extends('layouts.book', ['step' => 'confirmation'])

@section('booking_content')

    @include('book.partials.voucher', ['booking' => $booking])

@endsection