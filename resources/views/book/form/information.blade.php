@extends('layouts.book', ['step' => 'information'])

@section('booking_content')

    <h5>Отправка заявки на бронирование</h5>
    <div class="well">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <img src="{{$tour->images->first()->small_src}}" class="img-responsive pull-left" style="height: 100px;"/>
                </div>
                <div class="pull-left" style="margin-left: 20px;">
                    <span style="font-weight: bold;">
                        {{$tour->name}}
                    </span>

                    <div style="margin-top: 8px;">
                        <i class="mdi mdi-calendar-clock mdi-24px mdi-dark mdi-inactive"></i>
                        <span>Дней: {{$trip->start_at->diffInDays($trip->finish_at)}}. Начало: {{$trip->start_at->formatLocalized('%d %B, %a')}}</span>
                    </div>

                    <div style="margin-top: 8px;">
                        <i class="mdi mdi-star-circle mdi-24px mdi-dark mdi-inactive"></i>
                        <span>Организатор: {{$trip->tour->user->name}}</span>
                    </div>
                </div>

            </div>
        </div>
    </div>

    {!! Form::open(['action' => ['BookController@create']]) !!}
    {{ csrf_field() }}

    {!! Form::hidden('trip_id', $trip->id) !!}

    <div class="row">
        <div class="col-md-12 o-members">

            <style>
                .o-member .form-group {
                    padding: 8px 0;
                }

                .o-member .form-group label {
                    width: 260px;
                    text-align: right;
                    padding-right: 12px;
                }
                .o-member .form-group input[type=text],
                .o-member .form-group input[type=email],
                .o-member .form-group input[type=number],
                .o-member .form-group textarea {
                    width: 260px;
                }
            </style>
            <div class="form-group o-member">
                <div class="form-group form-inline @if ($errors->has('name')) has-warning @endif">
                    {!! Form::label('name', 'Имя') !!}
                    {!! Form::text('name', $name, ['class' => 'form-control', 'placeholder' => 'Ваше имя', 'autofocus' => 'autofocus']) !!}
                </div>
                <div class="form-group form-inline @if ($errors->has('email')) has-warning @endif">
                    {!! Form::label('email', 'E-mail') !!}
                    {!! Form::email('email', $email, ['class' => 'form-control', 'placeholder' => 'Ваш e-mail']) !!}
                </div>
                <div class="form-group form-inline @if ($errors->has('phone_number')) has-warning @endif">
                    {!! Form::label('phone_number', 'Номер телефона') !!}
                    {!! Form::number('phone_number', $phoneNumber, ['class' => 'form-control', 'placeholder' => 'Ваш номер телефона, 8xxxxxxxxxx']) !!}
                </div>
                <div class="form-group form-inline">
                    <label for="is_sms_enabled">Присылать смс уведомления</label>
                    <input id="is_sms_enabled" type="checkbox" name="is_sms_enabled" checked="checked"/>
                </div>
                <div class="form-group form-inline @if ($errors->has('members')) has-warning @endif">
                    {!! Form::label('members', 'Количество участников') !!}
                    {!! Form::number('members', null, ['class' => 'form-control', 'placeholder' => 'Участников']) !!}
                </div>
                <div class="form-group form-inline @if ($errors->has('description')) has-warning @endif">
                    {!! Form::label('description', 'Особые пожелания', ['style' => 'vertical-align: top;']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Пожелания по питанию, трансферу, проживанию...', 'style' => 'height: 100px;']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 22px;">
        <div class="col-md-12">
            {!! Form::submit('Отправить заявку', ['class' => 'btn btn-success', 'style' => 'margin-left: 268px;']) !!}
        </div>
    </div>
    <div class="row" style="margin-top: 12px;">
        <div class="col-md-12">
            <p class="text-muted" style="margin-left: 268px;">
                Отправка заявки является предварительной и не обязует к оплате или бронированию
            </p>
        </div>
    </div>

    {!! Form::close() !!}


@endsection