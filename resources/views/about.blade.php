@extends('layouts.app')


@section('content')

    <?php
        $splashImage = Illuminate\Support\Collection::make([
            '/images/rafting-679719.jpg',
            '/images/happy-woman-1452389.jpg',
            '/images/nature-1283693.jpg',
            '/images/ski-mountaineering-1375016.jpg',
        ])->random();
    ?>

    <style>
        .splash {
            position: relative;
            background: #141d27 url({{$splashImage}}) no-repeat left center;
            -o-background-size: cover;
            -webkit-background-size: cover;
            color: #fff;
            text-align: center;
            height: 250px;
        }
        .splash .header-container {
            position: absolute;
            top: 90px;
            width: 100%;
        }
        .splash h1 {
            text-shadow: 0 3px 3px rgba(0, 0, 0, 0.8);
        }
    </style>
    <div class="splash">
        <div class="header-container text-center">
            <h1>О нас</h1>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="page-header">
                    <h2>Wiild.ru</h2>
                </div>
                <p>
                    Онлайн-сервис бронирования активных туров по России
                </p>
            </div>
        </div>
    </div> <!-- /container -->

@endsection