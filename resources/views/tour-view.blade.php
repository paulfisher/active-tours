@extends('layouts.app')

@section('content')


    <style>
        .tour-info-label-container {
            margin-bottom: 12px;
        }
        .tour-info-label {
            color: #A7A7A7;
            text-transform: uppercase;
            font-weight: bold;
            font-size: 12px;
        }
        .tour-info-dates {
            font-size: 20px;
            font-weight: bold;
        }
        .tour-info-price {
            font-size: 44px;
        }
        .carousel-link {
            width: 135px;
            margin-top: 4px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>
                    <small><a href="/area">Алтай /</a></small>
                    Сплав по р. Большая Кокшага
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="tour-carousel" class="carousel slide" data-ride="carousel" data-interval="10000">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active"><img src="/images/tour/kamchatka.jpg" alt="..."></div>
                        <div class="item"><img src="/images/tour/kamchatka2.jpg" alt="..."></div>
                        <div class="item"><img src="/images/tour/kavkaz.jpg" alt="..."></div>
                        <div class="item"><img src="/images/tour/ural.jpg" alt="..."></div>
                        <div class="item"><img src="/images/tour/karelia2.jpeg" alt="..."></div>
                    </div>

                    <a class="left carousel-control" href="#tour-carousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left glyphicon-menu-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#tour-carousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right glyphicon-menu-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>


                <div class="carousel-links-container">
                    <img class="carousel-link" data-target="#tour-carousel" data-slide-to="0" src="/images/tour/kamchatka.jpg"/>
                    <img class="carousel-link" data-target="#tour-carousel" data-slide-to="1" src="/images/tour/kamchatka2.jpg"/>
                    <img class="carousel-link" data-target="#tour-carousel" data-slide-to="2" src="/images/tour/kavkaz.jpg"/>
                    <img class="carousel-link" data-target="#tour-carousel" data-slide-to="3" src="/images/tour/ural.jpg"/>
                    <img class="carousel-link" data-target="#tour-carousel" data-slide-to="4" src="/images/tour/karelia2.jpeg"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="o-tour-dates-price-container">
                    <div class="row tour-info-label-container">
                        <div class="col-md-6">
                            <span class="tour-info-label">Даты поездки</span>
                        </div>
                        <div class="col-md-6">
                            <span class="tour-info-label">Стоимость на 1 человека</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <span class="tour-info-dates" onclick="document.location.href=document.location.href+'#trips'">30 апреля – 6 мая</span>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <span class="tour-info-price">1329</span>
                                <span class="tour-info-currency">руб.</span>
                            </div>
                            <a href="javascript: void 0;" class="btn btn-warning btn-lg">Забронировать сейчас</a>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-12">
                        <p class="tour-short-description">
                            А знаете ли Вы, что именно в первых числах мая великое озеро Байкал освобождается ото льда? Это удивительное явление мечтают увидеть сотни путешественников и фотографов со всего мира! Представьте только, если Вы на минуту закроете глаза и прислушаетесь, то услышите, как потрескивает и шуршит лед у края берега…
                        </p>
                    </div>
                </div>
                <hr/>
                <div class="row tour-info-label-container">
                    <div class="col-md-12">
                        <span class="tour-info-label">Рейтинг</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <strong>4.8/5 Очень хорошо!</strong>
                        <a href="#testimonials" class="btn btn-link btn-xs">Читать отзывы</a>
                    </div>
                </div>

            </div>
        </div>

        <hr/>
        <div class="row">
            <div class="col-md-12">
                <p>
                    Утро встречает Вас ярким и теплым солнышком, природа просыпается. Вы слышите? Это крики чаек…
                    <br/>
                    После завтрака Вы сможете самостоятельно погулять и еще раз насладиться утренней свежестью майского дня.
                    <br/>
                    А кто-то, возможно, захочет искупаться...
                </p>
                <p>
                    <ul class="list-group">
                        <li class="list-group-item">Размер группы: от 2 до 16 человек</li>
                        <li class="list-group-item">Продолжительность: 8 дней</li>
                    </ul>
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <strong>Что включено в стоимость:</strong>
                <ul class="list-group">
                    <li class="list-group-item">Проживание в «Горном лагере»</li>
                    <li class="list-group-item">Трехразовое питание</li>
                    <li class="list-group-item">Выбранные активности</li>
                    <li class="list-group-item">Внутримаршрутный транспорт</li>
                </ul>
            </div>
            <div class="col-md-6">
                <strong>Что оплатить дополнительно:</strong>
                <ul class="list-group">
                    <li class="list-group-item">Проезд в Краснодар и обратно (стоимость авиабилетов — от 5 125 руб. в обе стороны, ж/д-билетов в один конец — от 2 328 руб.)</li>
                    <li class="list-group-item">Трансфер Краснодар — Каменномостский — Краснодар (2 500 руб./машина в одну сторону)</li>
                    <li class="list-group-item">Проживание на базе «Золотой олень» (+ 3000 руб./чел.)</li>
                    <li class="list-group-item">Проживание на базе «Зеленый гай» (+ 6000 руб./чел.)</li>
                </ul>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-2">
                <h4>1 день</h4>
            </div>
            <div class="col-md-10">
                <div class="well">
                    <b>Барнаул</b>
                    <ul>
                        <li>Утреннее прибытие в Барнаул. Встреча в аэропорту.</li>
                        <li>Размещение в гостинице «Барнаул» в центре города</li>
                        <li>Завтрак в ресторане отеля, отдых</li>
                        <li>Посещение Алтайского государственного краеведческого музея</li>
                    </ul>
                    <p>Музейное собрание насчитывает свыше 150 тыс. экспонатов, среди которых единственная в мире модель паровой машины, изобретенная в Барнауле И.И. Ползуновым в 1763 г. Особый интерес вызывают археологические находки, рассказывающие об истории древнего Алтая, предметы быта, отражающие этническую, социальную принадлежность населению региона. Также интересны историко-техническая (в том числе военные реликвии), нумизматическая, минералогическая коллекции, естественнонаучные сборы, представляющие биоразнообразие и природные ресурсы Алтая.</p>
                    <ul>
                        <li>Посещение музея аптечного дела на Алтае «Горная аптека»</li>
                        <li>Поездка в ресторан</li>
                        <li>Ужин</li>
                        <li>Возвращение в гостиницу</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <h4>2 день</h4>
            </div>
            <div class="col-md-10">
                <div class="well">
                    <b>Чемал</b>
                    <ul>
                        <li>Переезд по Чуйскому тракту через его высшую точку, перевал Семинский (1739 м над у. м.) до этно-природного парка Уч-Энмек</li>
                        <li>Национальный обед</li>
                    </ul>
                    <p>Вы услышите уникальное горловое пение и игру на национальных музыкальных инструментах. Некоторые исполнители — лауреаты международных фестивалей. </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <h4>3 день</h4>
            </div>
            <div class="col-md-10">
                <div class="well">
                    <b>Домой</b>
                    <p>Национальный музей республики Алтай (430 км на автомобиле)</p>
                </div>
            </div>
        </div>
        <hr id="testimonials"/>
        @include('partials.testimonials')
        <hr id="trips"/>
        <div class="row">
            <div class="col-md-12">
                <h5>Другие даты этого тура</h5>
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                14 августа - 1 сентября
                            </div>
                            <div class="col-md-4">
                                23,000 руб.
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-sm btn-warning" href="/tour">Забронировать</a>
                            </div>
                        </div>
                    </div>
                    <div class="list-group-item">
                        <div class="row">
                            <div class="col-md-4">
                                5 сентября - 12 сентября
                            </div>
                            <div class="col-md-4">
                                23,000 руб.
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-sm btn-warning" href="/tour">Забронировать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /container -->

@endsection

@push('footer_scripts')
<style>
    .o-tour-dates-price-container {
        overflow: hidden;
        background: #ffffff;
        z-index: 999;
        width: 555px;
    }
    .o-tour-dates-price-container.affix {
        top: 20px;
        overflow: hidden;
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
    }
    .o-tour-dates-price-container.affix-bottom {
        position: absolute;
        -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        -moz-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1);
    }
</style>

    <script>
        /*
        * top: previewContainer.offset().top,
         bottom: $('footer').outerHeight(true) + 82
         */
        $(function() {
            var container = $('.o-tour-dates-price-container');
            container.affix({
                offset: {
                    top: container.offset().top,
                    bottom: 1250
                }
            });
        });
    </script>
@endpush