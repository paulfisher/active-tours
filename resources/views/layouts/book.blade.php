@extends('layouts.empty', ['hideNavbar' => true])

@section('content')
    <style>
        .step-link.active {
            font-weight: bold;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="well">
                    {{--<div class="row" style="background-color: #fff;padding: 4px;margin-top:12px;">
                        <div class="col-md-4 text-center">
                            <a href="{{route('book.tours', $ownerId)}}" class="step-link @if ($step == 'tours') active @else text-muted @endif">Выбор тура</a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#" class="step-link @if ($step == 'information') active @else text-muted @endif">Контактные данные</a>
                        </div>
                        <div class="col-md-4 text-center">
                            <a href="#" class="step-link @if ($step == 'confirmation') active @else text-muted @endif">Подтверждение</a>
                        </div>
                    </div>--}}


                    <div class="row">
                        <div class="col-md-12">
                            @yield('booking_content')
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /container -->
@endsection