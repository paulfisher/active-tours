<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Активные туры по России</title>

    <link href="{{ elixir('compiled/app.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ elixir('compiled/plugins.css') }}" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('head')
    <meta name="interkassa-verification" content="6e02fc91ec58aa506e4fe10717d2f1ad" />
</head>
<body>

    @include('partials.navbar')

    @if (Session::has('flash_success'))
        <div class="container">
            <div class="alert alert-success">{{Session::get('flash_success')}}</div>
        </div>
    @endif
    @if (Session::has('flash_error'))
        <div class="container">
            <div class="alert alert-danger">{{Session::get('flash_error')}}</div>
        </div>
    @endif

    @yield('content')

    @include('partials.footer')

    <script src="{{ elixir('compiled/app.js') }}" type="text/javascript"></script>

    @stack('footer_scripts')
    @include('partials.body_end_scripts')
</body>
</html>