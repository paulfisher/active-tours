<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть минимум 6 символов и соответствовать повторному',
    'reset' => 'Ваш пароль успешно сброшен',
    'sent' => 'Мы отправили ссылку на сброс пароля на ваш email',
    'token' => 'Ссылка на изменение пароля неверная',
    'user' => "Пользователь не найден",

];
