<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Пользователь с такими email-пароль не найден',
    'throttle' => 'Слишком много попыток. Попробуйте через :seconds секунд.',

];
